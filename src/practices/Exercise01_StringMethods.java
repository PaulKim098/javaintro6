package practices;

import utilities.ScannerHelper;



public class Exercise01_StringMethods {
    public static void main(String[] args) {
        /*


         */
        System.out.println("\n--------Task-1---------\n");

        String str = ScannerHelper.getString();

        System.out.println("The String given is = "  + str);


        /*
        Check if the given String has at least one character
        If it does not have any character, print "The String given is empty"
        If it is not empty, then print the length of the String with a message -> The length = {LENGTH}
         */
        System.out.println("\n--------Task-2--------\n");

        if(str.isEmpty()) System.out.println("The String given is empty");
        else System.out.println("The length = " + str.length());

        System.out.println(str.isEmpty() ? "The String given is empty" : "The length = " + str.length());

        if(str.length() == 0) System.out.println("The String given is empty");
        else System.out.println("The length = " + str.length());

        /*
        Task-3
        if it is not empty, print the first character with message -> "The first character = {RESULT}
        if it is empty, "The is no character in this String"

         */

        System.out.println("\n--------Task-3--------\n");
        if (str.isEmpty()) System.out.println("The is no character in this String");
        else System.out.println("The first character = " + str.charAt(0));

//        if (!str.isEmpty()) System.out.println("The first character = " + str.charAt(0));
//        else System.out.println("The is no character in this String");

        /*
        Task-4
        if it is not empty, print the last character with message -> "The last character = {RESULT}
        if it is empty, "The is no character in this String"

         */
        System.out.println("\n--------Task-4--------\n");
//        if (str.isEmpty()) System.out.println("The is no character in this String");
//        else System.out.println("The last character = " + str.charAt(str.length()-1));

        if (!str.isEmpty()) System.out.println("The last character = " + str.charAt(str.length()-1));
        else System.out.println("The is no character in this String");

        //System.out.println(str.isEmpty() ? "The is no character in this String" : "The last character = " + str.charAt(str.length()-1));

        /*
        -Check if the String contains any vowel letter
        -if it has any vowel, then print "This String has vowel"
        -else, print "This String does not have vowel"
        Vowels = a, e, i, o, u, A, E, I, O, U

        Hello -> This String has vowel
        bcd   -> This String does not have vowel
        "  "  -> This String does not have vowel
        "abc" -> This String has vowel
         */

        System.out.println("\n--------Task-5--------\n");

        str = str.toLowerCase();

//        if (str.toLowerCase().contains("a") || str.toLowerCase().contains("e") || str.toLowerCase().contains("i") ||
//                str.toLowerCase().contains("o") || str.toLowerCase().contains("u")) System.out.println("This String has a vowel");
//        else System.out.println("This String does not have vowel");

        if (str.contains("a") || str.contains("e") || str.toLowerCase().contains("i") ||
                str.toLowerCase().contains("o") || str.toLowerCase().contains("u")) System.out.println("This String has a vowel");
        else System.out.println("This String does not have vowel");


    }
}

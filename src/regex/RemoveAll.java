package regex;

import utilities.ScannerHelper;

import java.util.regex.Pattern;

public class RemoveAll {
    public static void main(String[] args) {
        //counting vowels (without regex)
        String str = ScannerHelper.getString();

        int vowelCounter = 0;
        for (char c : str.toCharArray()) {
            if(Character.toLowerCase(c) == 'a' || Character.toLowerCase(c) == 'e'|| Character.toLowerCase(c) == 'i' ||
                    Character.toLowerCase(c) == 'o' || Character.toLowerCase(c) == 'u') vowelCounter++;
        }
        System.out.println("The word has " + vowelCounter + " vowels");

        //counting vowels (with regex)
        str = str.replaceAll("[^aeiouAEIOU]", "");
        System.out.println("The word has " + str.length() + " vowels");


        //Phone number verification

        String phoneNumberRegex = "[(]?[0-9]{3}[)]?-[0-9]{3}-[0-9]{4}"; // \\([0-9]{3}\\)-[0-9]{3}-[0-9]{4}
        System.out.println(Pattern.matches(phoneNumberRegex, "(123)-234-9988"));

        //(xxx)-xxx-xxxx
        //(xxx) -> [0-9]{3}
        //xxx
        //xxxx
    }
}

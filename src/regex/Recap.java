package regex;

public class Recap {
    public static void main(String[] args) {
        /*
        RegEx: Regular Expression
        RegEx: Pattern that we create to search in some texts


        Meta Character
        .           -> any character
        \d          -> [0-9]         -> any digit
        \D          -> [^0-9]        -> any character but not digit
        \s          -> space character
        \S          -> any character but not space
        \w          -> [0-9a-zA-Z_]
        \W          -> space + specials except _


         */


        String str = "123 St Chicago IL 12345.";

        //[^0-9] not digits
        System.out.println(str.replaceAll("[^\\d]", "")); //12312345 more ideal
        System.out.println(str.replaceAll("[^0-9]", "").length()); //8


        //Count letters  -> 11 -> [^a-zA-Z]
        System.out.println(str.replaceAll("[^a-zA-Z]", "")); //StChicagoIL
        System.out.println(str.replaceAll("[^a-zA-Z]", "").length());

        //Special characters -> 1 -> [^0-9a-zA-Z ]
        System.out.println(str.replaceAll("[0-9a-zA-Z]", "")); //.
        System.out.println(str.replaceAll("[0-9a-zA-Z]", "").length()); //1

        // Spaces -> 4
        System.out.println("\n-------------------------\n");
        System.out.println(str.replaceAll("\\S", "")); // "    "
        System.out.println(str.replaceAll("\\S", "").length()); // 4

        // spaces + specials
        System.out.println("\n-------------------------\n");
        System.out.println(str.replaceAll("\\w", "")); // "    ."
        System.out.println(str.replaceAll("\\w", "").length()); // 5




    }
}

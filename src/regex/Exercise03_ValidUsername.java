package regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise03_ValidUsername {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter a username:");
        String userName = scan.nextLine();
        String regex = "[a-zA-Z0-9]{5,10}";
        //String input = ScannerHelper.getAString();

        if (Pattern.matches(regex, userName)) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");

        //Way 2

        if (userName.matches(regex)) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");

        //Way 3
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(userName);

        if (matcher.matches()) System.out.println("Valid Username");
        else System.out.println("Error! Username must be 5 to 10 characters long and can only contain letters and numbers");
    }
}

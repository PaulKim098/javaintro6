package regex;


import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise02_CountWords {
    public static void main(String[] args) {
        String sentence = ScannerHelper.getSentence();
        String[] arr = sentence.split(" "); // ["hello,", "", "my", "", "name", "", "john"]

        System.out.println("This sentence contains " + arr.length + " words");
        for (String s : arr) {
            System.out.println(s);
        }


        System.out.println("\n--------REGEX WAY--------\n");
        //Regex Way
        //String sentence = ScannerHelper.getSentence();
        Pattern pattern = Pattern.compile("[A-Za-z]{2,}");
        Matcher matcher = pattern.matcher("hello,  my name is john");

        int countWords = 0;

        while(matcher.find()){
            System.out.println(matcher.group());
            countWords++;
        }
        System.out.println("This sentence contains " + countWords + " words.");

    }
}

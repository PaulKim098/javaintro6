package regex;

import utilities.ScannerHelper;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Exercise01_PatternMatcher {
    public static void main(String[] args) {

        String input = ScannerHelper.getString();
        //System.out.println(Pattern.matches("[a-z0-9_-]{3,10}", input));
        // java -> true
        // "i love java" -> false
        // "1a2B3" -> false
        // "tech-global-school" -> false


        Pattern pattern = Pattern.compile("Java");// compiles String regex into a Regex pattern
        //Pattern pattern = Pattern.compile(" ");// compiles String regex into a Regex pattern
        Matcher matcher = pattern.matcher("I love Java, Java is fun");

        System.out.println(pattern); // prints out compiled regex pattern as a Pattern

        System.out.println(pattern.toString()); // prints out compiled regex pattern as a String
        System.out.println(pattern.pattern()); // prints out compiled regex pattern as a String

        System.out.println(matcher.matches()); // prints out a boolean if it matches the Pattern

        System.out.println();

        int count = 0;
        while(matcher.find()){
            count++;
            System.out.println(matcher.group());
            System.out.println(matcher.start());
            System.out.println(matcher.end());
        }
        System.out.println("Java count: " + count); // "word count: " + (count + 1));



    }
}

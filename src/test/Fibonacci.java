package test;

import java.util.Arrays;
import java.util.Scanner;

public class Fibonacci {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(fibonacci()));
    }

    public static int[] fibonacci(){
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter desired length of the fibonacci sequence");
        int num = scan.nextInt();

        int sum;
        int num1 = 0, num2 = 1;

        int[] array = new int[num];

        for (int i = 0; i < num; i++) {
            array[i] = num1;
            sum = num1 + num2;
            num1 = num2;
            num2 = sum;
        }
        return array;





    }
}

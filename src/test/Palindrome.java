package test;

public class Palindrome {
    public static void main(String[] args) {
        palindrome("civic");
        palindrome("robot");
        palindrome("T");
        palindrome("Tat");
        palindrome("");

        String some = palindrome("T");
        System.out.println(some);

        String some1 = palindrome("");
        System.out.println(some1);
    }

    public static String palindrome(String word) {
        String reverse = "";

        if (word.length() - 1 < 0) return ("\nThis word does not have 1 or more characters");
        else {
            for (int i = word.length() - 1; i >= 0; i--) {
                reverse += word.charAt(i);
            }

        }
        return (reverse.equals(word) ? "\nThis word is palindrome" : "\nThis word is not palindrome");
    }
}

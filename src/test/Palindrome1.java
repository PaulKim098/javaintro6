package test;

public class Palindrome1 {
    public static void main(String[] args) {
        palindrome("civic");
        palindrome("T");
        palindrome("TaT");
        palindrome("boo");

        palindrome("");

    }

    public static void palindrome(String word) {
        String reverse = "";

        if (word.length() < 1) System.out.println(("This word does not have 1 or more characters"));
        else {
            for (int i = word.length() - 1; i >= 0; i--) {
                reverse += word.charAt(i);
            }
            System.out.println((reverse.equals(word) ? "This is a palindrome" : "This is not a palindrome"));
        }

    }
}
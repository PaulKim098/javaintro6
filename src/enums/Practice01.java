package enums;

public class Practice01 {
    public static void main(String[] args) {
        /*
        Write a program that prints "it is a work day" if the day is a weekday
        And it prints "it is OFF today" if the day is a weekend day

        SUNDAY, SATURDAY        -> it is OFF today
        MONDAY-FRIDAY           -> it is work day

         */

        DaysOfTheWeek dayByUser = DaysOfTheWeek.FRIDAY;

        switch (dayByUser){
            case SATURDAY:
            case SUNDAY:
                System.out.println("it is OFF today");
                break;
            case MONDAY:
            case TUESDAY:
            case WEDNESDAY:
            case THURSDAY:
            case FRIDAY:
                System.out.println("it is work day");
                break;
            default:
                throw new RuntimeException("No such enum value!!!");
        }
    }
}

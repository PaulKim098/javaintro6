package primitives;

public class Characters {
    public static void main(String[] args) {
        /*
        Floating numbers: 10.5, 23.32423, 10000.238764823764

        float   -> 4 bytes
        double  -> 8 bytes
         */

        char c1 = 'A';
        char c2 = ' ';

        System.out.println(c1); // A
        System.out.println(c2); //

        char myFavCharacter = 'P';
        System.out.println(myFavCharacter);

        System.out.println("My favorite char = " + myFavCharacter);
    }
}

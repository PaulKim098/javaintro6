package gitDemo;

public class Demo {
    public static void main(String[] args) {
        String firstName = "Paul";
        String lastName = "Kim";
        System.out.println(firstName + " " + lastName);

        /*
        Task-01
        Write out your favorite color and print it out.
        Print out "My favorite color is " ".
         */

        String favoriteColor = "Blue";
        System.out.println("My favorite color is " + favoriteColor);
    }
}

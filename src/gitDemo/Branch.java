package gitDemo;

public class Branch {
    public static void main(String[] args) {
        /*
        Branching: We create branches for individual members, so that they can get started with coding
                    Ex: feature/teamname/userstory#-context
                        feature/AutomationTester/US1307-Retry-Functionality

        git branch nameOfTheBranch
        git checkout -b nameOfTheBranch

        Task-01
        Create a new branch called feature/JavaTeam/US01-LearningAboutMergeConflicts

         */
    }
}

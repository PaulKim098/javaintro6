package utilities;

import java.util.Scanner;

public class ScannerHelper {
    static Scanner input = new Scanner(System.in);

    // Write a method that asks and return a name from user
    public static String getFirstName(){
        System.out.println("Please enter a first name:");
        return input.nextLine();
    }

    public static String getLastName(){
        System.out.println("Please enter a last name:");
        return input.nextLine();
    }

    public static String getFullName(){
        System.out.println("Please enter a full name:");
        return input.nextLine();
    }

    public static int getAge(){
        System.out.println("Please enter an age:");
        int age = input.nextInt();
        input.nextLine();

        return age;
    }

    public static int getNumber(){
        System.out.println("Please enter a number:");
        int num = input.nextInt();
        input.nextLine();

        return num;
    }

    public static String getString(){
        System.out.println("Please enter a String:");
        String str = input.nextLine();
        return str;
    }

    public static String getFavBook(){
        System.out.println("Please enter your favorite book name:");
        String bookName = input.nextLine();
        return bookName;
    }

    public static String getFavQuote(){
        System.out.println("Please enter your favorite quote:");
        String quote = input.nextLine();
        return quote;
    }

    public static String getAddress(){
        System.out.println("Please enter your full address:");
        String address = input.nextLine();
        return address;
    }

    public static String getFavCountry(){
        System.out.println("Please enter your favorite country:");
        String country = input.nextLine();
        return country;
    }

    public static String getSentence(){
        System.out.println("Please enter a sentence:");
        String sent = input.nextLine();
        return sent;
    }

    public static String getAWord(){
        System.out.println("Please enter a word:");
        String word = input.nextLine();
        return word;
    }
}

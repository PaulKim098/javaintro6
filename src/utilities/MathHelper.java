package utilities;

public class MathHelper {

    //Write a public static method named max() and returns the greatest number of 3 numbers
    /*
        return or void?                     ->   return
        static or non-static                -> static
        what is the name of the method      -> max()
        what it returns?                    ->  int
        Does it take arguments?             -> 3 int arguments
     */

    public static int maxOfThree(int a, int b, int c){
        return Math.max(Math.max(a, b), c);
    }

    //Write a public static method named sum() and returns the sum of 2 numbers
    public static int sum(int num1, int num2){
        return (num1 + num2);
    }

    public static int sum(int num1, int num2, int num3){
        return sum(num1, num2) + num3;
    }

    public static double sum(double a, double b){
        return (a + b);
    }

    //Write a public static method named product() and returns the product of 2 numbers
    public static int product(int num1, int num2){
        return (num1 * num2);
    }

    //Write a public static method named square() and returns the square of a number
    public static int square(int num1){
        return (num1^2);
    }
}

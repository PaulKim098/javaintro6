package string_methods;

public class _15_IsEmpty_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a boolean
        non-static
        takes a String as an argument
         */

        String emptyStr = "";
        String word = "Hello";

        System.out.println("First String is empty = " + emptyStr.isEmpty()); //true
        System.out.println("Second String is empty = " + word.isEmpty()); //false
    }
}

package string_methods;

import utilities.ScannerHelper;

public class Exercise05 {
    public static void main(String[] args) {
        String str = ScannerHelper.getString();

        if (str.startsWith("a") && str.endsWith("e")) System.out.println(true);
        else System.out.println(false);

        System.out.println(str.startsWith("a") && str.endsWith("e"));
    }
}

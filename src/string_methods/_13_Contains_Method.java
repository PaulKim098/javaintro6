package string_methods;

import utilities.ScannerHelper;

public class _13_Contains_Method {
    public static void main(String[] args) {
        /*
        return type
        returns boolean
        non-static
        takes a String as an argument
         */
        String name = "John Doe";

        boolean containJohn = name.toLowerCase().contains("john");
        System.out.println(containJohn);

        // write a program to ask the user for a String
        // return true if this String is a sentence and false if it is a single word
        // ask the user using ScannerHelper
        // check if String contains spaces
        // check if String ends with period

        String str = ScannerHelper.getString().trim();

        System.out.println(str.contains(" ") && str.endsWith("."));



    }
}

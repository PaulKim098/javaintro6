package string_methods;

public class _14_Replace_Method {
    public static void main(String[] args) {
        /*
        return type
        returning a String
        non-static method
        takes 2 arguments: either 2 chars or 2 Strings
         */

        String str = "ABC123";

        System.out.println(str); // ABC123

        System.out.println(str.replace("ABC", "abc")); // abc123

        System.out.println(str.replace("2", "")); //ABC13

        str = str.replace("ABC", "abc");
        System.out.println(str); // abc123
    }
}

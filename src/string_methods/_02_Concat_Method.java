package string_methods;

public class _02_Concat_Method {
    public static void main(String[] args) {

        /*
        1. return type
        2. returning String
        3. non-static
        4. its takes a String
         */
        String str1 = "Tech";
        String str2 = "Global";

        System.out.println(str1.concat(str2));
        System.out.println("Tech".concat("Global"));
    }
}

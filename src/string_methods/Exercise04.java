package string_methods;

public class Exercise04 {
    public static void main(String[] args) {
        /*
        assume you have the String "I go to TechGlobal"
        extract every word from the String into other Strings and print them out on different lines

        Expected Output:
        I
        go
        to
        TechGlobal
         */

        String tech = "I go to TechGlobal";

        System.out.println(tech.charAt(0)); // tech.substring(0,1) // I
        System.out.println(tech.substring(2,4)); //go
        System.out.println(tech.substring(5,7)); //to
        System.out.println(tech.substring(8)); //TechGlobal

    }
}

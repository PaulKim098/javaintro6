package string_methods;

public class ExerciseSubstring {
    public static void main(String[] args) {
        String str = "The best is Java";

        String subStr = str.substring(12);
        System.out.println(subStr);

        //Second way

        subStr = str.substring(str.indexOf("Java"));
        System.out.println(subStr);

        //Third way
        subStr = str.substring(str.lastIndexOf(' ') + 1);
        System.out.println(subStr);

        //Fourth way
        subStr = str.substring(str.lastIndexOf(' ')).trim();
        System.out.println(subStr);
    }
}

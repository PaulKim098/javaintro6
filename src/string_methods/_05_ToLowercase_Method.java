package string_methods;

public class _05_ToLowercase_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a String
        non-static
        does not take arguments
         */
        String str1 = "JAVA IS FUN";

        System.out.println(str1); //JAVA IS FUN
        System.out.println(str1.toLowerCase()); //java is fun

        char c = 'A';

        System.out.println(String.valueOf(c).toLowerCase()); //a
    }
}

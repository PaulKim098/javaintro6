package string_methods;

public class _06_ToUppercase_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a String
        non-static
        does not take an argument
         */

        System.out.println("HelloWorld".toUpperCase()); //HELLOWORLD

        System.out.println("".toUpperCase());

        String s1 = "HELLO";
        String s2 = "hello";


        if(s1.toUpperCase().equals(s2.toUpperCase())) System.out.println("EQUAL");
        else System.out.println("NOT EQUAL");
    }
}

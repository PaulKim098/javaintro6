package string_methods;

public class _10_Trim_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a String
        non-static
        takes no argument
         */

        String str = "      TechGlobal     ";

        System.out.println(str); //       TechGlobal
        System.out.println(str.trim()); //TechGlobal

        String str2 = "   Hel lo Wor ld   ";

        System.out.println(str); //    Hel lo Wor ld
        System.out.println(str.trim()); //HelloWorld
    }
}

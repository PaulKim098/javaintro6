package string_methods;

import java.util.Arrays;

public class _16_toCharArray_Method {
    public static void main(String[] args) {

        String name = "John";

        char[] charsOfName = name.toCharArray(); // ACCEPTABLE

        System.out.println(Arrays.toString(charsOfName)); // [J, o, h, n]

        //Print the element at the index of 1 -> 0

        System.out.println(charsOfName[1]); // o

        // Print the length of the array

        System.out.println(charsOfName.length);

        // Print each character of the array

        for (char c : charsOfName) { //
            System.out.println(c);
        }




    }
}

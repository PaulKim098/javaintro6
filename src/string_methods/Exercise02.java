package string_methods;

import utilities.ScannerHelper;

public class Exercise02 {
    public static void main(String[] args) {
        /*
        Write a Java program that asks user to enter their favorite
        book name and quote

        And store answers of user in different Strings

        Finally, print the length of those Strings with proper messages
         */

        String bookName = ScannerHelper.getFavBook();
        String quote = ScannerHelper.getFavQuote();

        System.out.println(bookName.length());
        System.out.println(quote.length());
    }
}

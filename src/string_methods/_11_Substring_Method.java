package string_methods;

public class _11_Substring_Method {
    public static void main(String[] args) {
        /*

         */

        String str = "I love java a lot";

        String firstWord = str.substring(0,1); //I -> str.charAt(0)
        String secondWord = str.substring(2,6); //love
        String thirdWord = str.substring(7,11); //java

        System.out.println(firstWord);
        System.out.println(secondWord);
        System.out.println(thirdWord);

        System.out.println(str.substring(7));
    }
}

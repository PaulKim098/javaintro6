package string_methods;

public class _03_Equals_Method {
    public static void main(String[] args) {
        /*
        1. return type
        2. returning a boolean
        3. non-static
        4. takes an object as an argument but in our case takes a String
         */

        String str1 = "Tech";
        String str2 = "Global";
        String str3 = "tech";
        String str4 = "TechGlobal";

        boolean isEquals = str1.equals(str2);

        System.out.println(isEquals); // false

        System.out.println(str1.equals(str3)); // false

        System.out.println((str1.concat(str2)).equals(str4));

    }
}

package string_methods;

public class _09_Length_Method {
    public static void main(String[] args) {
        /*
        return type
        return an int
        non-static
        takes no arguments
         */

        String str = "I am learning java and it is fun";

        int lengthOfStr = str.length();
        //System.out.println(lengthOfStr); with "TechGlobal" // 10

        System.out.println(lengthOfStr); // 32

        String str2 = "";
        System.out.println(str2.length()); // 0

    }
}

package string_methods;

public class _07_CharAt_Method {
    public static void main(String[] args) {
        /*
        return type
        returns a char
        non-static
        takes an index as an argument
         */

        String name = "Bilal";

        char firstChar = name.charAt(0); // B
        char secondChar = name.charAt(0); // i
        char thirdChar = name.charAt(0); // l
        char fourthChar = name.charAt(0); // a
        char fifthChar = name.charAt(0); // l

        System.out.println(name);

        System.out.println(firstChar);
        System.out.println(secondChar);
        System.out.println(thirdChar);
        System.out.println(fourthChar);
        System.out.println(fifthChar);

        String str = "HelloWorld";
        System.out.println(str.charAt(9));
//        System.out.println(str.charAt(10)); // OutOfBoundsException
//        System.out.println(str.charAt(-3)); // OutOfBoundsException

    }
}

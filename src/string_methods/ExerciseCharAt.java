package string_methods;

import utilities.ScannerHelper;

public class ExerciseCharAt {
    public static void main(String[] args) {
        String str = "TechGlobal";// last index is 9 | length is 10
        String str2 = "Hello World";// last index is 10 | length is 11
        String str3 = "I really love java";// last index is 17 | length is 18

        System.out.println(str.charAt(4)); //G

        //print out the last character of the String
        System.out.println(str.charAt(9)); //l

        System.out.println(str.charAt(str.length()-1)); //l


        /*
        Ask the user to enter a string and print out the last character of that string

        program: Please enter a string
        User: hello
        program: 0
         */
        String getStr = ScannerHelper.getString().trim();

        System.out.println(getStr.charAt(getStr.length()-1));

    }
}

package strings;

public class UnderstandingString {
    public static void main(String[] args) {

        String s1;
        s1 = " TechGlobal School";

        String s2 = "is the best"; // declaration of s2 and initialization of s2 as is the best;

        System.out.println("\n--------CONCAT USING +----------\n");
        String s3 = s1 + " " + s2; // concatenation using

        System.out.println("\n---------EX-1---------\n");
        String wordPart1 = "le";
        String wordPart2 = "ar";
        String wordPart3 = "ning";
        String fullWord = wordPart1 + wordPart2 + wordPart3;
        System.out.println("le" + "ar" + "ning");
        System.out.println(fullWord);

    }
}

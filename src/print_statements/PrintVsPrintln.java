package print_statements;

public class PrintVsPrintln {
    public static void main(String[] args){
        // I will write code statements here

        System.out.println("Hello World");

        //Today is Sunday
        System.out.println("Today is Sunday");

        //print method
        System.out.print("Hello World");
        System.out.print("Today is Sunday");
        System.out.print("John Doe");

    }
}

package singleton;

public class Driver {

    private static Driver driver; // null

    private Driver(){
        //default constructor
    }

    //Create a method that instantiate a Driver object and return it
    public static Driver getDriver(){
        if(driver == null) driver = new Driver();

        return driver;

    }
}

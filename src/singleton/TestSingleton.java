package singleton;

public class TestSingleton {
    public static void main(String[] args) {
        Phone p1 = Phone.getPhone();
        Phone p2 = Phone.getPhone();
        Phone p3 = Phone.getPhone();

        System.out.println(p1); // singleton.Phone@1b6d3586
        System.out.println(p2); // singleton.Phone@1b6d3586
        System.out.println(p3); // singleton.Phone@1b6d3586


        Driver d1 = Driver.getDriver();
        Driver d2 = Driver.getDriver();
        Driver d3 = Driver.getDriver();

        System.out.println(d1); // singleton.Driver@4554617c
        System.out.println(d2); // singleton.Driver@4554617c
        System.out.println(d3); // singleton.Driver@4554617c

    }
}

//  Normally we would be able to create an object and have this memory address but if constructor is private then you cannot
//        Phone p1 = new Phone();
//        Phone p2 = new Phone();
//        Phone p3 = new Phone();
//
//        System.out.println(p1); // singleton.Phone@1b6d3586
//        System.out.println(p2); // singleton.Phone@4554617c
//        System.out.println(p3); // singleton.Phone@74a14482
package exceptions.custom_exceptions;

public class Exercise01 {
    public static void main(String[] args) {
        System.out.println(checkAge(-1));
        System.out.println(checkAge(-1));
        System.out.println(checkAge(-1));
        System.out.println(checkAge(-1));
        System.out.println(checkAge(-1));
        System.out.println(checkAge(-1));
    }


    /*
    Write a method that takes an int as an argument and return true or false
    The method name is checkAge

    if the age is more than or equals 16 -> return true
    if the age is less than 16 -> return false

    if the age is less than 1 or more than 120 -> throw RunTimeException
     */

    public static boolean checkAge(int age){
        if(age >= 16 && age <= 120) return true;
        else if(age > 0 && age < 16) return false;
        else throw new RuntimeException("The age cannot be less than or more than 120!!!");
    }

    /*
    Create a method that takes as an argument to state the day of the week
    1 - Sunday
    2 - Monday
    7 - Saturday
    method name = isCheckInHours()
    Method return true or false

    if it is a valid day -> return true

    if the input is not in the range of 1-7, then throw an exception with message
    "The input does not represent any day!!!"
     */

    public static boolean isCheckInHours(int dayOfWeek){

        if(dayOfWeek >= 1 && dayOfWeek <= 7) return true;
        else throw new RuntimeException("The input does not represent any day!!!");

    }



}

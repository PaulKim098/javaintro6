package loops;

import utilities.ScannerHelper;

public class Exercise12_ReverseString {
    public static void main(String[] args) {
        /*
        Write a program that reads a name from user
        Reserve the name and print it back

        Test data:
        James

        Expected Output:
        semaJ

        Test data:
        John

        Expected Output:
        nhoJ
         */

        String str = ScannerHelper.getString();
        String reverse = "";

        for (int i = str.length()-1; i >= 0; i--) {
            reverse += str.charAt(i);
        }

        System.out.println(reverse);

    }
}

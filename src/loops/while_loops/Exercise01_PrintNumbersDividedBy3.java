package loops.while_loops;

public class Exercise01_PrintNumbersDividedBy3 {
    public static void main(String[] args) {
        /*
        Write a program that prints all the numbers divided by 3, starting from 1 to 100 (both included)

        3
        6
        9
        .
        .
        .
        .
        96
        99
         */

        System.out.println("\n-----while loop and preferred way-----\n");
        int num = 1;

        while (num <= 100){
            if (num % 3 == 0) System.out.println(num);
            num++;
        }

        System.out.println("\n-----while loop another way-----\n");
        int j = 3;

        while (j <= 100){
            System.out.println(j);
            j += 3;
        }
    }
}

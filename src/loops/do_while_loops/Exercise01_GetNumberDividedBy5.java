package loops.do_while_loops;

import utilities.ScannerHelper;

public class Exercise01_GetNumberDividedBy5 {
    public static void main(String[] args) {

        /*
        Write a program that asks user to enter a number.
        Keep asking til they enter a number that can be divided by 5.

        Please enter a number:
        3

        Please enter a number:
        4

        Please enter a number:
        25

        End of the program.
         */

        System.out.println("\n-----d0 while loop-----\n");

        int num;

        do{
            num = ScannerHelper.getNumber();
        } while (num % 5 != 0);
        System.out.println("End of the program");

        System.out.println("\n-----while loop-----\n");

        // Create an infinite loop, keep asking user to enter a number but breaj the loop when you get a number that can be divides by 5

        while(true){
            int n1 = ScannerHelper.getNumber();
            if (n1 % 5 == 0){
                System.out.println("End of the program");
                break;
            }
        }

        System.out.println("\n-----for loop-----\n");

        for(; ;){
            int n2 = ScannerHelper.getNumber();
            if (n2 % 5 == 0){
                System.out.println("End of the program");
                break;
            }
        }

    }
}

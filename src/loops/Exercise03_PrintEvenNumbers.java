package loops;

public class Exercise03_PrintEvenNumbers {
    public static void main(String[] args) {

        /*
        Write a Java program to print only even numbers from 0 to 10 (0 and 10 are included)

        Expected output:
        0
        2
        4
        6
        8
        10
         */

        System.out.println("------first way------");
        for (int i = 0; i <= 10; i++) {
            if (i % 2 == 0) System.out.println(i);
        }


        // second way but risk to create mistakes
        System.out.println("------second way------");
        for (int i = 0; i <= 10; i += 2) {
            System.out.println(i);
        }

        // third way but not advised
        System.out.println("------third way------");
        for (int i = 0; i <= 5; i++) {
            System.out.println(i * 2);
        }
    }
}

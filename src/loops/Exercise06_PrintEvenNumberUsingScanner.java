package loops;

import utilities.ScannerHelper;

public class Exercise06_PrintEvenNumberUsingScanner {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter 2 different positive numbers
        Print all the even numbers bt given numbers by user in ascending order
        3, 10 -> 
        4 
        6 
        8 
        10
        
        7, 2  -> 
        2
        4 
        6
        
        
         */
//        Scanner scan = new Scanner(System.in);
        
//        System.out.println("Please enter 2 different positive numbers");
//        int num1 = scan.nextInt();
//        int num2 = scan.nextInt();

//        int max = Math.max(num1, num2);
//        int min = Math.min(num1, num2);
//
//        if (min < max) {
//            for (int i = min; i <= max; i++) {
//                if(i % 2 == 0) System.out.println(i);
//            }
//        }

        int first = ScannerHelper.getNumber();
        int second = ScannerHelper.getNumber();

        for (int i = Math.min(first, second); i <= (Math.max(first, second)); i++) {
            if (i % 2 == 0) System.out.println(i);
        }

    }
}

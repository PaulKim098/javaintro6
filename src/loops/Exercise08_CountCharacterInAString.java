package loops;

import utilities.ScannerHelper;

public class Exercise08_CountCharacterInAString {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String
        Count how many A or a letter you have in the given String

        Apple  -> 1
        Banana -> 2
        Strawberry -> 1
        John -> 0

         */

        String str = ScannerHelper.getString();

        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'A' || str.charAt(i) == 'a') count++;
        }
        System.out.println(count);

    }
}

package loops;

public class UnderstandLoops {
    public static void main(String[] args) {

        /*
        for loops syntax

        for(initializations; termination; update){
            // block of code to be executed
        }

        initialization -> start
        termination -> stop condition
        update -> increasing or decreasing -> increment and decrement operators
         */

        //Print Hello World 5 times/20 times/ and now with a ! at the end

        for (int i = 0; i < 20; i++) {
            System.out.println("Hello World!");
        }

        System.out.println("----Decrement----");

        for (int i = 5; i > 0; i--) {
            System.out.println("Hello World!");
        }

    }
}
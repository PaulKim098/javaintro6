package loops;

import utilities.ScannerHelper;

public class Exercise07_PrintCharacterInAString {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String
        Print each character of the String in a separate line

        "Hello"
        H
        e
        l
        l
        o


        "Hi"
        H
        i

        "Hi John"
        H
        i

        J
        o
        h
        n
         */

        String str = ScannerHelper.getString();

        for (int i = 0; i < str.length(); i++) {
            System.out.println(str.charAt(i));
        }

    }
}

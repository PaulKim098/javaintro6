package scanner_class;

import java.util.Scanner;

public class ScannerFirstAndLastName {
    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter your first name:");
        String firstName = scan.next();

        System.out.println("Please enter your last name:");
        String lastName = scan.next();

        System.out.println("Your full name is " + firstName + " " + lastName);
    }
}

package projects;

import utilities.ScannerHelper;

public class Project04 {
    public static void main(String[] args) {


        System.out.println("\n---------Task-1---------\n");

        String str = ScannerHelper.getString();

        if (str.length() >= 8){
            if (str.length() % 2 == 0)
                System.out.println(str.substring(str.length()-4) + str.substring(4, str.length()-4)
                        + str.substring(0,4));
        } else System.out.println("This String does not not 8 characters");

        System.out.println("\n---------Task-2---------\n");

        String word = ScannerHelper.getString();

        int last = word.lastIndexOf(" ") + 1;

        System.out.println(word.contains(" ") ? (word.substring(last)) + (word.substring(word.indexOf(" "), last))
                + (word.substring(0, word.indexOf(" "))) : "This sentence does not have 2 or more words to swap");

        System.out.println("\n---------Task-3---------\n");

        String str1 = "These books are so stupid";
        String str2 = "I like idiot behaviors";
        String str3 = "I had some stupid te-shirts in the past and also some idiot look shoes";

        System.out.println(str1.replace("stupid", "nice"));
        System.out.println(str2.replace("idiot", "nice"));
        System.out.println(str3.replace("stupid", "nice").replace("idiot","nice"));

        System.out.println("\n---------Task-4---------\n");

        String fName = ScannerHelper.getFirstName();

        if (fName.length() >= 2){
            if (fName.length() % 2 == 0) System.out.println(fName.substring(fName.length() / 2 - 1, fName.length()/2 + 1));
            else System.out.println(fName.charAt(fName.length() / 2));
        } else System.out.println("Invalid input!!!");


        System.out.println("\n---------Task-5---------\n");

        String country = ScannerHelper.getFavCountry();

        System.out.println(country.length() >= 5 ? country.substring(2, country.length()-2) : "Invalid input!!!");

        System.out.println("\n---------Task-6---------\n");
        String address = ScannerHelper.getAddress();

        System.out.println(address.replace('a', '*').replace('A', '*')
                .replace('e', '#').replace('E', '#')
                .replace('i', '+').replace('I', '+')
                .replace('o', '$').replace('O', '$')
                .replace('u', '@').replace('U', '@'));

        System.out.println("\n---------Task-7---------\n");
        String sent = ScannerHelper.getSentence().trim();

        if (sent.contains(" ")){
            System.out.println("This sentence has " + sent.split(" ").length + " words.");
        } else System.out.println("This sentence does not have multiple words.");

    }
}

//        System.out.println("\n---------Task-8---------\n");
//
//        int random1 = RandomNumberGenerator.getRandomNumber(0, 25);
//        int random2 = RandomNumberGenerator.getRandomNumber(0, 25);
//
//        String ascend = "";
//
//        int min8 = Math.min(random1, random2);
//        int max8 = Math.max(random1, random2);
//
//        for (int i = min8; i <= max8; i++) {
//        if (i % 5 != 0) {
//        ascend += i;
//        if (i + 1 == max8 && i % 5 == 1)
//        break;
//        else {
//        ascend += " - ";
//        }
//        }
//        }
//
//        System.out.println("Min random num = " + min8);
//        System.out.println("Max random num = " + max8);
//        System.out.println(ascend);
//
//        System.out.println("\n---------Task-9---------\n");
//
//        int posNum = ScannerHelper.getNumber();
//
//        for (int i = 1; i <= posNum; i++){
//        if (i % 6 == 0) System.out.println("FooBar");
//        else if (i % 3 == 0) System.out.println("Bar");
//        else if (i % 2 == 0) System.out.println("Foo");
//        else System.out.println(i);
//        }
//
//        System.out.println("\n---------Task-10---------\n");
//        String word1 = ScannerHelper.getString();
//
//        if (word1.length() < 1){
//        System.out.println("This word does not have 1 or more characters");
//        } else {
//        for (int i = 0; i < word1.length()-1; i++) {
//
//        }
//        }
//
//
//        System.out.println("\n---------Task-11---------\n");
//        String getStr = ScannerHelper.getString();
//
//        int count = 0;
//
//        if (getStr.length() == 0 || (!getStr.contains("a") || !getStr.contains("A"))){
//        System.out.println("This sentence does not have any characters");
//        } else {
//        for (int i = 0; i < getStr.length(); i++) {
//        if (getStr.charAt(i) == 'a' || getStr.charAt(i) == 'A') count++;
//        }
//        System.out.println("This sentence has " + count + " a or A letters.");
//        }

package projects;

import utilities.ScannerHelper;

import java.util.Random;

public class Project05 {
    public static void main(String[] args) {

        System.out.println("\n---------Task 1---------\n");

        String sentence = ScannerHelper.getSentence();
        int countSpaces = 1;

        if (!sentence.contains(" ")) System.out.println("\nThis sentence does not have multiple words");
        else {
            for (int i = 0; i < sentence.length() ; i++) {
                if (Character.isWhitespace(sentence.charAt(i))) countSpaces++;
            }
            System.out.println("\nThis sentence has " + countSpaces + " words.");
        }


//        for (int i = 0; i < sentence.length() ; i++) {
//            if (sentence.length() <= 2) break;
//            else if (Character.isWhitespace(sentence.charAt(i))) countSpaces++;
//        }
//        System.out.println(sentence.length() <= 2 ? "\nThis sentence does not have multiple words" :
//                "\nThis sentence has " + countSpaces + " words.");

        System.out.println("\n---------Task 2---------\n");

        Random rand = new Random();

        int num1 = rand.nextInt(26);
        int num2 = rand.nextInt(26);

        System.out.println(num1);
        System.out.println(num2);

        String solution = "";

        for (int i = Math.min(num1, num2); i < Math.max(num1, num2); i++) {
            if (i % 5 == 0) continue;
            else solution+= i + " - ";
        }
        System.out.println(solution.substring(0, solution.length() - 3));

        System.out.println("\n---------Task 3---------\n");

        sentence = ScannerHelper.getSentence();
        int countA = 0;

        if (!sentence.contains("a") || !sentence.contains("A"))
            System.out.println("This sentence does not have any characters");
        else {
            for (int i = 0; i < sentence.length(); i++) {
                if (sentence.charAt(i) == 'a' || sentence.charAt(i) == 'A') countA++;
            }
            System.out.println("This Sentence has " + countA + " a or A letters.");
        }

        System.out.println("\n---------Task 4---------\n");

        String word = ScannerHelper.getAWord();

        String reverse = "";

        if (word.length()-1 < 0) System.out.println("\nThis word does not have 1 or more characters");
        else {
            for (int i = word.length()-1; i >= 0; i--) {
                reverse += word.charAt(i);
            }
            System.out.println(reverse.equals(word) ? "\nThis word is palindrome" : "\nThis word is not palindrome");

        }

        System.out.println("\n---------Task 5---------\n");

        //second way
        int rows = 9;
        int spaces = rows - 1;

        for (int i = 0; i < rows; i++) {
            String row = "";
            for (int a = 0; a < spaces; a++) {
                row += "   ";
            }

            for (int b = 0; b < (2 * i) + 1; b++) {
                row += " * ";
            }

            System.out.println(row);
            spaces--;
        }

//      //first way
//        int star = 1;
//        int rows = 9;
//        int spaces = rows - 1;
//
//        for (int i = 0; i < rows; i++) {
//            for (int a = 0; a < spaces; a++) {
//                System.out.print("   ");
//            }
//
//            for (int b = 0; b < star; b++) {
//                System.out.print(" * ");
//            }
//            System.out.println();
//            spaces--;
//            star += 2;

    }
}
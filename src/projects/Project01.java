package projects;

public class Project01 {
    public static void main(String[] args) {

        System.out.println("\n---------Task-1-------\n");
        /*
        -Store your name in a String variable called name
        -Print the variable with a proper message
        -EX/ My name is = name
        NOTE: Uppercase's, lowercase's and spaces are important
         */

        String name = "Paul Kim";

        System.out.println("My name is = " + name);

        System.out.println("\n---------Task-2-------\n");
        /*
        -Create different char variables for each of your name letter and store them in separate variables as
        nameCharacter1, nameCharacter2 and so on.
        -Print variables with proper messages
        -EX/Name letter 1 is = nameCharacter1
            Name letter 2 is = nameCharacter2

         */

        char nameCharacter1 = 'P', nameCharacter2 = 'a', nameCharacter3 = 'u', nameCharacter4 = 'l';

        System.out.println("Name letter 1 is = " + nameCharacter1);
        System.out.println("Name letter 2 is = " + nameCharacter2);
        System.out.println("Name letter 3 is = " + nameCharacter3);
        System.out.println("Name letter 4 is = " + nameCharacter4);

        System.out.println("\n---------Task-3-------\n");
        /*
        -Create different String variables to store info like myFavMovie, myFavSong, myFavCity, myFavActivity, myFavSnack.
        -Print variables with proper messages
        -EX/My favorite movie is = myFavMovie
         */

        String myFavMovie, myFavSong, myFavCity, myFavActivity, myFavSnack;

        myFavMovie = "The Shawshank Redemption";
        myFavSong = "Golden Hour by JVKE";
        myFavCity = "Seoul";
        myFavActivity = "Watching Netflix";
        myFavSnack = "Cookies and Cream Ice Cream";

        System.out.println("My favorite movie is = " + myFavMovie);
        System.out.println("My favorite song is = " + myFavSong);
        System.out.println("My favorite city is = "+ myFavCity);
        System.out.println("My favorite activity is = " + myFavActivity);
        System.out.println("My favorite snack = " + myFavSnack);


        System.out.println("\n---------Task-4-------\n");
        /*
        -Create different int variables to store info like myFavNumber, numberOfStatesIVisited, numberOfCountriesIVisited.
        -Print variables with proper messages
        -EX/My favorite number is = myFavNumber
         */

        int myFavNumber, numberOfStatesIVisited, numberOfCountriesIVisited;

        myFavNumber = 24;
        numberOfStatesIVisited = 8;
        numberOfCountriesIVisited = 2;

        System.out.println("My favorite number is = " + myFavNumber);
        System.out.println("The number of states I visited is = " + numberOfStatesIVisited);
        System.out.println("The number of countries I visited is = " + numberOfCountriesIVisited);

        System.out.println("\n---------Task-5-------\n");
        /*
        -Create a boolean called amIAtSchoolToday
        -Assign true to this variable if you are at campus today
        -Assign false to this variable if you are joining online today
        -Print variable value with a proper message using println() method
        EX/ I am at school today = amIAtSchoolToday
         */

        boolean amIAtSchoolToday = false;

        System.out.println("I am at school today = " + amIAtSchoolToday);

        System.out.println("\n---------Task-6-------\n");
        /*
        -Create a boolean called isWeatherNiceToday
        -Assign true to this variable if it is above 60F today
        -Assign false to this variable if it is below or equal to 60F today
        -Print variable value with a proper message using println() method
        -EX/ Weather is nice today = isWeatherNiceToday
         */

        boolean isWeatherNiceToday = false;

        System.out.println("Weather is nice today = " + isWeatherNiceToday);
    }
}

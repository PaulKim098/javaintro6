package projects;


import java.util.ArrayList;
import java.util.Arrays;

public class Project07 {
    public static void main(String[] args) {
        System.out.println("\n------Task-01------\n");
        System.out.println(countMultipleWords(new String[]{"foo", "", " ", "foo bar", "java is fun", " ruby "}));

        System.out.println("\n------Task-02------\n");
        System.out.println(removeNegatives(new ArrayList<>(Arrays.asList(2, -5, 6, 7, -10, -78, 0, 15))));

        System.out.println("\n------Task-03------\n");
        System.out.println(validatePassword(""));
        System.out.println(validatePassword("abcd"));
        System.out.println(validatePassword("abcd1234"));
        System.out.println(validatePassword("Abcd1234"));
        System.out.println(validatePassword("Abcd123!"));

        System.out.println("\n------Task-04------\n");
        System.out.println(validateEmailAddress("a@gmail.com"));
        System.out.println(validateEmailAddress("abc@g.com"));
        System.out.println(validateEmailAddress("abc@@gmail.com"));
        System.out.println(validateEmailAddress("abcd@gmail.com"));
        System.out.println(validateEmailAddress("ab@gmail.com"));
    }

    public static int countMultipleWords(String[] arr){

        int count = 0;

        for (String s : arr) {
            String str = s.trim();
            if (str.split(" ").length > 1)
                count++; //str.isEmpty() && !str.matches("^\\s+$"
        }
        return count;
    }

    public static ArrayList<Integer> removeNegatives(ArrayList<Integer> list){

        ArrayList<Integer> newList = new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            Integer num = list.get(i);
            if (num >= 0) newList.add(i);
        }
        return newList;
    }

    public static Boolean validatePassword(String password){


        int upper = 0;
        int lower = 0;
        int digit = 0;
        int special = 0;

        if (!password.contains(" ") && password.length() >= 8 && password.length() <= 16) {
            for (int i = 0; i < password.length(); i++) {
                if (Character.isUpperCase(password.charAt(i))) upper++;
                else if (Character.isLowerCase(password.charAt(i))) lower++;
                else if (Character.isDigit(password.charAt(i))) digit++;
                else if (!Character.isLetterOrDigit(password.charAt(i))) special++;
            }
        } else return false;
        return (upper >= 1 && lower >= 1 && digit >= 1 && special >= 1);
    }


    public static Boolean validateEmailAddress(String email){
        int atIndex = email.indexOf('@'); // >2
        int dotIndex = email.indexOf('.'); // >5

        if(email.contains(" ") && email.length() < 6) return false;

        if(atIndex <2 || dotIndex < atIndex + 3 || dotIndex >= email.length() - 2) return false;

        if(email.indexOf('@', atIndex + 1) != -1) return false;

        return true;
    }
}

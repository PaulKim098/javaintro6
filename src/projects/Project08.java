package projects;

import java.util.Arrays;

public class Project08 {
    public static void main(String[] args) {

        System.out.println("\n--------Task-1--------\n");
        System.out.println(findClosestDistance(new int[] {4}));
        System.out.println(findClosestDistance(new int[] {4, 8, 7, 15}));
        System.out.println(findClosestDistance(new int[] {10, -5, 20, 50, 100}));


        System.out.println("\n--------Task-2--------\n");
        System.out.println(findSingleNumber(new int[] {2}));
        System.out.println(findSingleNumber(new int[] {5, 3, -1, 3, 5, 7, -1}));
        System.out.println(findSingleNumber(new int[] {21, 21, 7, 14, 14, 3, 3, 5}));

        System.out.println("\n--------Task-3--------\n");
        System.out.println(findFirstUniqueCharacter("Hello"));
        System.out.println(findFirstUniqueCharacter("abc abc d"));
        System.out.println(findFirstUniqueCharacter("abab"));

        System.out.println("\n--------Task-4--------\n");
        System.out.println(findMissingNumber(new int[] {2, 4}));
        System.out.println(findMissingNumber(new int[] {2, 3, 1, 5}));
        System.out.println(findMissingNumber(new int[] {4, 7, 8, 6}));

    }

    //Task-1
    public static int findClosestDistance(int[] arr) {

//        if (arr.length < 2) {
//            return -1;
//        }
//
//        int minDiff = Integer.MAX_VALUE;
//        for (int i = 0; i < arr.length - 1; i++) {
//            for (int j = i + 1; j < arr.length; j++) {
//                int diff = Math.abs(arr[i] - arr[j]);
//                if (diff < minDiff) {
//                    minDiff = diff;
//                }
//            }
//        }
//        return minDiff;
        
//        int smallestDiff = Integer.MAX_VALUE;
//
//        if(arr.length >= 2){
//            for (int i = 0; i < arr.length; i++) {
//                for (int j = i + 1; j < arr.length; j++) {
//                    int diff = Math.abs(Math.max(arr[i], arr[j]) - Math.min(arr[i], arr[j]));
//                    if(diff < smallestDiff) smallestDiff = diff;
//                }
//            }
//        } else smallestDiff = -1;
//        return smallestDiff;

        Arrays.sort(arr);
        if (arr.length < 2) return -1;

        int diff = Integer.MAX_VALUE;

        for (int i = 0; i + 1 < arr.length; i++) {
            int currentDiff = arr[i + 1] - arr[i];
            diff = Math.min(currentDiff, diff);
        }
        return diff;
    }

    //Task-2
    public static int findSingleNumber(int[] arr){
//        Arrays.sort(arr);
//        for (int i = 0; i < arr.length; i += 2) {
//            if (arr[i] != arr[i + 1]) {
//                return arr[i];
//            }
//        }
//        return arr[arr.length - 1];

        Arrays.sort(arr);
        if (arr.length == 1) return arr[0];
        int i = 0;
        while (i < arr.length - 1){
            if (arr[i] != arr[i + 1]) return arr[i];

            i += 2;
        }
        return arr[i];
    }

    //Task-3
    public static char findFirstUniqueCharacter(String str){
        for (int i = 0; i < str.length() ; i++) {
            boolean unique = true;
            for (int j = 0; j < str.length(); j++) {
                if(i != j && str.charAt(i) == str.charAt(j)){
                    unique = false;
                    break;
                }
            }
            if (unique) return str.charAt(i);
        }
        return '\0';

//        int count = 0;
//        for (int i = 0; i < str.length() -1; i++) {
//            char c = str.charAt(i);
//            if (str.indexOf(c) == str.lastIndexOf(c)) return c;
//        }
//        return 0;


    }

    //Task-4
    public static int findMissingNumber(int[] arr){
        Arrays.sort(arr);
        for (int i = 0; i < arr.length - 1; i++) {
            if (arr[i + 1] - arr[i] > 1) {
                return arr[i] + 1;
            }
        }
        return -1;
    }

}

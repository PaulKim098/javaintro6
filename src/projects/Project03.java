package projects;

import java.util.Random;

public class Project03 {
    public static void main(String[] args) {

        System.out.println("\n------Task-1------\n");
        String s1 = "24", s2 = "5";

        System.out.println("The sum of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("The subtraction of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) - Integer.parseInt(s2)));
        System.out.println("The division of " + s1 + " and " + s2 + " = " + ((double)Integer.parseInt(s1) / Integer.parseInt(s2)));
        System.out.println("The multiplication of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("The remainder of " + s1 + " and " + s2 + " = " + (Integer.parseInt(s1) % Integer.parseInt(s2)));

        System.out.println("\n------Task-2------\n");
        Random r = new Random();

        int randNum = r.nextInt(35) + 1;

        if (randNum == 1 || randNum == 2 || randNum == 3 || randNum == 5 || randNum == 7 || randNum == 11 ||
                randNum == 13 || randNum == 17 || randNum == 19 || randNum == 23 || randNum == 29 || randNum == 31){
            System.out.println(randNum + " IS A PRIME NUMBER");
        } else {
            System.out.println(randNum + " IS NOT A PRIME NUMBER");
        }

//        for (int i = 2; i <= randNum / 2; i++){
//            if (randNum % i == 0){
//                System.out.println(randNum + " IS NOT A PRIME NUMBER");
//            } else {
//                System.out.println(randNum + " IS A PRIME NUMBER");
//            }
//        }

        System.out.println("\n------Task-3------\n");

        int num1 = (int)(Math.random() * 50) + 1;
        int num2 = (int)(Math.random() * 50) + 1;
        int num3 = (int)(Math.random() * 50) + 1;

        System.out.println(num1);
        System.out.println(num2);
        System.out.println(num3);

        int max = Math.max(Math.max(num1, num2), num3);
        int min = Math.min(Math.min(num1, num2), num3);

        System.out.println("Lowest number is = " + min);
        if (num1 != max && num1 != min){
            System.out.println("Middle number is = " + num1);
        } else if (num2 != max && num2 != min) {
            System.out.println("Middle number is = " + num2);
        } else {
            System.out.println("Middle number is = " + num3);
        }
        System.out.println("Greatest number is = " + max);


        System.out.println("\n------Task-4------\n");
        char ch1 = '5';

        if (ch1 >= 'A' && ch1 <= 'Z' || ch1 >= 'a' && ch1 <= 'z'){
            if (ch1 >= 'A' && ch1 <= 'Z'){
                System.out.println("The letter is uppercase");
            } else {
                System.out.println("The letter is lowercase");
            }
        } else {
            System.out.println("Invalid character detected!!!");
        }

        System.out.println("\n------Task-5------\n");
        char cha = 'v';

        if ((cha >= 'A' && cha <= 'Z') || (cha >= 'a' && cha <= 'z')){
            if (cha == 'A' || cha == 'E' || cha == 'I' ||cha == 'O' || cha == 'U' || cha == 'a' || cha == 'e' || cha == 'i' ||
            cha == 'o' || cha == 'u') {
                System.out.println("The letter is a vowel");
            } else {
                System.out.println("The letter is a consonant");
            }
        } else {
            System.out.println("Invalid character detected!!!");
        }

        System.out.println("\n------Task-6------\n");
        char ch = 'v';

        if (ch >= 33 && ch <= 47 || ch >= 54 && ch <= 64 || ch >= 91 && ch <= 96 || ch >= 123 && ch <= 126){
            System.out.println("Special character is = " + ch);
        } else {
            System.out.println("Invalid character detected!!!");
        }

        System.out.println("\n------Task-7------\n");
        char c = 'v';

        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) System.out.println("Character is a letter");
        else if (c >= '1' && c <= '9') System.out.println("Character is a digit");
        else if (c >= 33 && c <= 47 || c >= 54 && c <= 64 || c >= 91 && c <= 96 || c >= 123 && c <= 126) System.out.println("Character is as special character");

    }
}

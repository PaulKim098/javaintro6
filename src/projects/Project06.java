package projects;

import java.util.Arrays;

public class Project06 {
    public static void main(String[] args) {

        System.out.println("\n-----Task 01------\n");
        int[] task01 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallestWithSort(task01);

        System.out.println("\n-----Task 02------\n");
        int[] task02 = {10, 7, 7, 10, -3, 10, -3};
        findGreatestAndSmallest(task02);

        System.out.println("\n-----Task 03------\n");
        //int[] task03 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallestWithSort(new int[] {10, 5, 6, 7, 8, 5, 15, 15});

        System.out.println("\n-----Task 04------\n");
        int[] task04 = {10, 5, 6, 7, 8, 5, 15, 15};
        findSecondGreatestAndSmallest(task04);

        System.out.println("\n-----Task 05------\n");
        String[] task05 = {"foo", "bar", "Foo", "bar", "6", "abc", "6", "xyz"};
        findDuplicatedElementsInAnArray(task05);

        System.out.println("\n-----Task 06------\n");
        String[] task06 = {"pen", "eraser", "pencil", "pen", "123", "abc", "pen", "eraser"};
        findMostRepeatedElementInAnArray(task06);
    }

    //Task 01 Method

    public static void findGreatestAndSmallestWithSort(int[] nums) {

        if (nums.length > 0) {
            Arrays.sort(nums);
            System.out.println("Smallest = " + nums[0]);
            System.out.println("Greatest = " + nums[nums.length - 1]);
        } else System.out.println("The Array contains no data");
    }

    //Task 02 Method

    public static void findGreatestAndSmallest(int[] nums) {

        if (nums.length > 0) {
            int greatest = Integer.MIN_VALUE, smallest = Integer.MAX_VALUE;
            for (int num : nums) {
                if (greatest < num) greatest = num;
                else if (smallest > num) smallest = num;
            }
            System.out.println("Smallest = " + smallest);
            System.out.println("Greatest = " + greatest);
        } else System.out.println("The Array contains no data");
    }

    //Task 03 Method

    public static void findSecondGreatestAndSmallestWithSort(int[] nums) {
        if (nums.length > 0) {
            Arrays.sort(nums);
            int minimum = nums[0];
            int maximum = nums[nums.length - 1];

            int secondMin = 0;
            int secondMax = 0;

            for (int i = 1; i < nums.length - 1; i++) {
                if (nums[i] != minimum) {
                    secondMin = nums[i];
                    break;
                }
            }
            for (int i = nums.length - 1; i > 0; i--) {
                if (nums[i] != maximum) {
                    secondMax = nums[i];
                    break;
                }
            }
            System.out.println("Second Smallest = " + secondMin);
            System.out.println("Second Greatest = " + secondMax);
        }
    }

    //Task 04 method

    public static void findSecondGreatestAndSmallest(int[] nums) {
        if (nums.length > 0) {
            int minimum = 100;
            int maximum = 0;

            int secondMinimum = 100;
            int secondMaximum = 0;

            for (int num : nums) {

                if (num > secondMaximum && num < maximum) secondMaximum = num;
                if (num > maximum) {
                    secondMaximum = maximum;
                    maximum = num;
                }

                if (num < secondMinimum && num > minimum) secondMinimum = num;
                if (num < minimum) {
                    secondMinimum = minimum;
                    minimum = num;
                }
            }
            System.out.println("Second Smallest = " + secondMinimum);
            System.out.println("Second Greatest = " + secondMaximum);
        }
    }

    // Task 05

    public static void findDuplicatedElementsInAnArray(String[] array) {
        System.out.println(Arrays.toString(array));
        boolean duplicate = false;

        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i].equals(array[j])) {
                    System.out.println(array[i]);
                    duplicate = true;
                }
            }
        }

    }

    // Task 06

    public static void findMostRepeatedElementInAnArray(String[] arr) {
        int maxCount = 0;
        String mostRepeated = "";

        for (int i = 0; i < arr.length; i++) {
            int count = 1;
            for (int j = arr.length - 1; j >= 0; j--) {
                if (arr[i].equals(arr[j])) {
                    count++;
                }
            }

            if (count > maxCount) {
                maxCount = count;
                mostRepeated = arr[i];
            }
        }

        System.out.println(mostRepeated);
    }

}

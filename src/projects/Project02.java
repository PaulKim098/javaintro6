package projects;

import java.util.Scanner;

public class Project02 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("\n---------Task-1-------\n");
        System.out.println("Please enter 3 numbers:");

        int num1 = scan.nextInt();
        int num2 = scan.nextInt();
        int num3 = scan.nextInt();

        System.out.println("The product of the numbers entered is = " + (num1 * num2 * num3));

        System.out.println("\n---------Task-2-------\n");
        scan.nextLine();
        System.out.println("Please enter your first name:");
        String firstName = scan.nextLine();

        System.out.println("Please enter your last name:");
        String lastName = scan.nextLine();

        System.out.println("Please enter your year of birth");
        int birthYear = scan.nextInt();

        System.out.println(firstName + " " + lastName + "'s age is = " + (2023- birthYear));


        System.out.println("\n---------Task-3-------\n");
        scan.nextLine();
        System.out.println("Please enter your full name:");
        String fullName = scan.nextLine();

        System.out.println("Please enter your weight in kg:");
        int weight = (int) Math.round(scan.nextDouble());

        System.out.println(fullName + "'s weight is = " + (weight * 2.205) + " lbs.");

        System.out.println("\n---------Task-4-------\n");
        scan.nextLine();
        System.out.println("Please enter your full name:");
        String fullName1 = scan.nextLine();
        System.out.println("What is your age?");
        int age1 = scan.nextInt();
        scan.nextLine();
        System.out.println("Please enter your full name:");
        String fullName2 = scan.nextLine();
        System.out.println("What is your age?");
        int age2 = scan.nextInt();
        scan.nextLine();
        System.out.println("Please enter your full name:");
        String fullName3 = scan.nextLine();
        System.out.println("What is your age?");
        int age3 = scan.nextInt();

        System.out.println(fullName1 + "'s age is " + age1 + ".");
        System.out.println(fullName2 + "'s age is " + age2 + ".");
        System.out.println(fullName3 + "'s age is " + age3 + ".");
        System.out.println("The average age is " + (age1 + age2 + age3) / 3);
        System.out.println("The eldest age is " + (Math.max(Math.max(age1, age2), age3)));
        System.out.println("The youngest age is " + (Math.min(Math.min(age1, age2), age3)));

    }
}

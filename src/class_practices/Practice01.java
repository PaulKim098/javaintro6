package class_practices;

import java.util.Random;

public class Practice01 {
    public static void main(String[] args) {

        System.out.println("\n-------Task01-------\n");
        Random rand = new Random();

        int randNum = rand.nextInt(51);
        System.out.println(randNum);
        System.out.println("The random number * 5 = " + randNum*5);

        System.out.println("\n-------Task02-------\n");

        int num1 = rand.nextInt(11);
        int num2 = rand.nextInt(11);

        System.out.println(num1);
        System.out.println(num2);

        System.out.println("Min number = " + Math.min(num1, num2));
        System.out.println("Max number = " + Math.max(num1, num2));
        System.out.println("Difference = " + Math.abs(num1 - num2));

        System.out.println("\n-------Task03-------\n");

        int r1 = rand.nextInt(51) + 50;

        System.out.println(r1);

        System.out.println("The random number % 10 = " + (r1 % 10));

        System.out.println("\n-------Task04-------\n");

        int rd1 = rand.nextInt(11);
        int rd2 = rand.nextInt(11);
        int rd3 = rand.nextInt(11);
        int rd4 = rand.nextInt(11);
        int rd5 = rand.nextInt(11);

        int points = 0;
        System.out.println(rd1);
        System.out.println(rd2);
        System.out.println(rd3);
        System.out.println(rd4);
        System.out.println(rd5);

        System.out.println((rd1 * 5)  + (rd2 * 4) + (rd3 * 3) + (rd4 * 2) + (rd5 * 1));

        System.out.println("\n-------Task05-------\n");
        int rando1 = rand.nextInt(26);
        int rando2 = rand.nextInt(26) + 25;
        int rando3 = rand.nextInt(26) + 50;
        int rando4 = rand.nextInt(26) + 75;

        System.out.println(rando1);
        System.out.println(rando2);
        System.out.println(rando3);
        System.out.println(rando4);

        int max = Math.max(Math.max(Math.max(rando1, rando2),rando3), rando4);
        int min = Math.min(Math.min(Math.min(rando1, rando2),rando3), rando4);

        System.out.println("Difference of max and min = " + Math.abs(max - min));
        System.out.println("Difference of second and third = " + Math.abs(rando2 - rando3));
        System.out.println("Average of all = " + (rando1 + rando2 + rando3 + rando4) / 4);

    }
}

package class_practices;

import java.util.Random;

public class Practice02 {
    public static void main(String[] args) {

        System.out.println("\n-------Task01-------\n");
        Random rand = new Random();

        int num = rand.nextInt(51);

        System.out.println(num);
        System.out.println(num >= 10 && num <= 25 ? true : false);

        System.out.println("\n-------Task02-------\n");
        int score = rand.nextInt(101);

//        String quarter;
//        switch (score / 25) {
//            case 0:
//                quarter = "1st";
//                break;
//            case 1:
//                quarter = "2nd";
//                break;
//            case 2:
//                quarter = "3rd";
//                break;
//            default:
//                quarter = "4th";
//                break;
//        }
//
//        // Determine which half the number is in using a switch case statement
//        String half;
//        switch (score / 50) {
//            case 0:
//                half = "1st";
//                break;
//            default:
//                half = "2nd";
//                break;
//        }
//
//
//        // Print the results
//        System.out.println(score + " is in the " + half + " half");
//        System.out.println(score + " is in the " + quarter + " quarter");

        //second way

        if (score <= 50){
            String half = "1st half";
            if(score <= 25) {
                String quarter = "1st quarter";
                System.out.println(score + " is in the " + half);
                System.out.println(score + " is in the " + quarter);
            }
            else{
                String quarter = "2nd quarter";
                System.out.println(score + " is in the " + half);
                System.out.println(score + " is in the " + quarter);
            }
        } else {
            String half = "2nd half";
            if (score <= 75){
                String quarter = "3rd quarter";
                System.out.println(score + " is in the " + half);
                System.out.println(score + " is in the " + quarter);
            }
            else{
                String quarter = "4th quarter";
                System.out.println(score + " is in the " + half);
                System.out.println(score + " is in the " + quarter);
            }
        }

        System.out.println("\n-------Task03-------\n");
        char c = '4';

        if (Character.isLetter(c)) System.out.println("Character is a letter");
        else if (Character.isDigit(c)) System.out.println("Character is a digit");

        /*
        if ((c >= 'A' && c <= 'Z') || (c >= 'a' && c <= 'z')) System.out.println("Character is a letter");
        else if (c >= '1' && c <= '9') System.out.println("Character is a digit");
        else if (c >= 33 && c <= 47 || c >= 54 && c <= 64 || c >= 91 && c <= 96 || c >= 123 && c <= 126) System.out.println("Character is as special character");
         */


    }
}

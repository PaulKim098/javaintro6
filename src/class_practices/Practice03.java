package class_practices;

import utilities.ScannerHelper;

public class Practice03 {
    public static void main(String[] args) {

        System.out.println("\n-------Task01-------\n");
        String str = ScannerHelper.getString();

        if (str.length() < 0){
            System.out.println("Length is zero");
        } else {
            System.out.println("Length is = " + str.length());
            System.out.println("First char is = " + str.length());
            System.out.println("Last char is = " + str.length());
            if (str.toLowerCase().contains("a") || str.toLowerCase().contains("e") || str.toLowerCase().contains("i") ||
            str.toLowerCase().contains("o") || str.toLowerCase().contains("u")) System.out.println("This String has vowel");
            else System.out.println("This String does not have vowel");
        }

        System.out.println("\n-------Task02-------\n");
        String str1 = ScannerHelper.getString();

        if (str1.length() < 3){
            System.out.println("Length is less than 3");
        } else {
            if (str1.length() % 2 == 0) {
                System.out.println(str1.substring(str1.length() / 2 - 1, str1.length() + 1));
            }
            else System.out.println(str1.charAt(str1.length() / 2));
        }

        System.out.println("\n-------Task03-------\n");
        String str2 = ScannerHelper.getString();

        if (str2.length() < 4) System.out.println("INVALID INPUT");
        else {
            System.out.println("First 2 characters are = " + str2.substring(0, 2));
            System.out.println("Last 2 characters are = " + str2.substring(str2.length()-2));
            System.out.println("The other characters are = " + str2.substring(2, str2.length()-2));
        }

        System.out.println("\n-------Task04-------\n");
        String str3 = ScannerHelper.getString();

        if (str3.length() < 2) System.out.println("Length is less than 2");
        else {
            System.out.println(str3.startsWith(str3.substring(str3.length() - 2)));
        }

        System.out.println("\n-------Task05-------\n");
        String strVal1 = ScannerHelper.getString();
        String strVal2 = ScannerHelper.getString();

        if (strVal1.length() < 2|| strVal2.length() < 2) System.out.println("INVALID INPUT");
        else {
            System.out.println(strVal1.substring(1, strVal1.length()-1) + strVal2.substring(1, strVal2.length()-1));
        }

        System.out.println("\n-------Task06-------\n");
        String s1 = ScannerHelper.getString();

        if (s1.length() < 4) System.out.println("INVALID INPUT");
        else{
            System.out.println(s1.startsWith("xx") || s1.endsWith("xx"));
        }



    }
}

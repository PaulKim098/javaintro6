package class_practices;

import utilities.ScannerHelper;

public class Practice04 {
    public static void main(String[] args) {
        System.out.println("\n-------Task01------\n");
        fooBar();

        System.out.println("\n-------Task02------\n");
        isNumMoreOrEqual10();

        System.out.println("\n-------Task03------\n");
        smallToBig();

        System.out.println("\n-------Task04------\n");
        countVowels();

        System.out.println("\n-------Task05------\n");
        fibonacci();

        System.out.println("\n-------Task06------\n");
        moreThan100();
    }
    
    public static void fooBar(){
        for (int i = 0; i < 10; i++) {
            if (i % 10 == 0) System.out.println("FooBar");
            else if (i % 2 == 0) System.out.println("Foo");
            else if (i % 5 == 0) System.out.println("Bar");
            else System.out.println(i);
        }
    }

    public static void isNumMoreOrEqual10(){

        int number;

        do {
            number = ScannerHelper.getNumber();
            if (number < 10) System.out.println("This number is not more than or equals to 10");
            else System.out.println("This number is more than or equals 10");
        } while (number < 10);

    }

    public static void smallToBig(){
        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        int start = Math.min(num1, num2);
        int end = Math.max(num1, num2);

        for (int i = start; i <= end; i++) {
            System.out.println(i);
        }
    }

    public static void countVowels(){
        String str = ScannerHelper.getString();

        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (str.toLowerCase().charAt(i) == 'a' || str.toLowerCase().charAt(i) == 'e' || str.toLowerCase().charAt(i) == 'i' ||
                    str.toLowerCase().charAt(i) == 'o' || str.toLowerCase().charAt(i) == 'u') count++;
        }
        System.out.println(count);
    }

    public static void fibonacci(){

        int sum = 0, fib = ScannerHelper.getNumber();
        int first = 0, second = 1;

        String result = "";

        for (int i = 0; i <= fib; i++) {
            result += first + " - ";
            sum = first + second;
            first = second;
            second = sum;
        }
        System.out.println(result.substring(0, result.length()-fib));
    }

    public static void moreThan100(){
        int num = 0;

        int i = 0;
        do {
            num += ScannerHelper.getNumber();
            i++;
        }while(num < 100);
        if (i > 1) System.out.println("Sum of the entered numbers is at least 100");
        else System.out.println("This number is already more than 100");
    }


}

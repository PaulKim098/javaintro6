package recursion;

public class TestRecursion {

    public static void main(String[] args) {

        System.out.println(sum1toNIterative(5)); // 15
        System.out.println(sum1toNIterative(4)); // 10
        System.out.println(sum1toNIterative(10)); // 55

        System.out.println();

        System.out.println(sum1toNRecursive(5)); // 15
        System.out.println(sum1toNRecursive(4)); // 10
        System.out.println(sum1toNRecursive(10)); // 55

        System.out.println(factorialRecursive(3));
        System.out.println(factorialRecursive(5));
        System.out.println(factorialRecursive(6));
    }

    /*
    Create iterative and recursive methods that are used to find the sum of
    the numbers from 1 to 5

    EXPECTED RESULT:
    15
     */

    public static int sum1toNIterative(int n){
        int sum = 0;

        for (int i = 0; i <= n; i++) {
            sum += i;
        }
        return sum;
    }

    public static int sum1toNRecursive(int n){
        if(n != 1) return n + sum1toNIterative(n-1);
        return 1;
    }

    /*
    Write a recursive method that finds factorial of the given positive numbers
    Factorial: numbers multiples with all the numbers from 1 to itself

    3   -> 1*2*3        -> 6
    5   -> 1*2*3*4*5    -> 120
    6   -> 1*2*3*4*5*6  -> 720
     */

    public static int factorialRecursive(int n){
        if(n != 1) return factorialRecursive(n-1) * n;
        return 1;

    }
}

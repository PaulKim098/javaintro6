package arrays;

import utilities.ScannerHelper;

import java.util.Arrays;

public class Exercise05_CountWords {
    public static void main(String[] args) {

        /*
        Write a program that asks user to enter a String
        And count how many words you have in the given String

        "Hello World"           -> 2
        "Java is fun"           -> 3
        "Today is a nice class with technical issues" -> 8


         */


        String str = ScannerHelper.getString();

        //System.out.println(str.split(" ").length);

        int countWords = 0;

        for (String word : str.split(" ")) {
            countWords++;
        }
        System.out.println(countWords);

        System.out.println("\n--------How to avoid miscounting when there is more spaces--------\n");

        String str3 = "Hello   World"; // 2 words

        String[] arr = str3.split(" ");

        System.out.println(Arrays.toString(arr));

        int actualWords = 0;

        for (String s : arr) {
            if (!s.isEmpty()) actualWords++;
        }

        System.out.println(actualWords);

    }
}

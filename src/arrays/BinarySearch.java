package arrays;

import java.util.Arrays;

public class BinarySearch {
    public static void main(String[] args) {

        System.out.println("\n--------Binary Search--------\n");
        int[] numbers = {3, 10, 8, 5, 5};

        //binary search cannot be used without sorting

        Arrays.sort(numbers); // [3, 5, 5, 8, 10]  0, 1, 2, 3, 4
        System.out.println(Arrays.binarySearch(numbers, 10)); // 4
        System.out.println(Arrays.binarySearch(numbers, 3)); // 0
        System.out.println(Arrays.binarySearch(numbers, 5)); // 2 - returns the index of 5 if found

        System.out.println(Arrays.binarySearch(numbers, 7)); // -4 -> [3, 5, 5, 7, 8, 10] -> -4
        System.out.println(Arrays.binarySearch(numbers, 15)); // -6 -> [3, 5, 5, 8, 10, 15] -> -6
        System.out.println(Arrays.binarySearch(numbers, 1)); // -1 -> [1, 3, 5, 5, 8, 10] -> -1

    }
}

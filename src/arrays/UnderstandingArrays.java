package arrays;

import java.util.Arrays;

public class UnderstandingArrays {
    public static void main(String[] args) {
        String[] cities = {"Chicago", "Miami", "Toronto"};

        //The number of elements in the array
        int sizeOfTheArray = cities.length;

        System.out.println(sizeOfTheArray); // 3
        //System.out.println(cities.length);


        //Get particular element from the array
        System.out.println(cities[1]); // Miami
        System.out.println(cities[0]); // Chicago
        System.out.println(cities[2]); // Toronto

        //HOW TO PRINT THE ARRAY -> [Chicago, Miami, Toronto]
        //1. Convert the array to a String
        //2. Print it with print method

        System.out.println(Arrays.toString(cities)); //[Chicago, Miami, Toronto]

        System.out.println("\n------for loop------\n");
        //HOW TO LOOP AN ARRAY
        for (int i = 0; i < cities.length; i++) {
            System.out.println(cities[i]);
        }

        System.out.println("\n------for each loop - enhanced loop------\n");
        //HOW TO LOOP AN ARRAY
        for (String city : cities) {
            System.out.println(city);
        }

    }
}

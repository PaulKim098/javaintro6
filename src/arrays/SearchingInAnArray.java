package arrays;

public class SearchingInAnArray {
    public static void main(String[] args) {

        int[] numbers = {3, 10, 8, 5, 5};

        // Check if this array has an element equals 7, print true if 7 exists, and false otherwise
        System.out.println("\n-------Loop way-------\n");
        boolean boo7 = false;

        for (int number : numbers) {
            if (number == 5) {
                boo7 = true;
                break;
            }
        }
        System.out.println(boo7);


        /*
        int count7 = 0;

        for (int number : numbers) {
            if(number == 7) count7++;
        }

        if(count7 == 0) System.out.println(false);
        else System.out.println(true);
         */
    }
}

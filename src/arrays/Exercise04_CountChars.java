package arrays;

import utilities.ScannerHelper;

public class Exercise04_CountChars {
    public static void main(String[] args) {
        /*
        Write a program that asks user to enter a String

        Count how many characters in the String are letters

        "abc1234" -> 3
        "       " -> 0
        "Hello"   -> 5

         */

        String str = ScannerHelper.getString();

        char[] chars = str.toCharArray();

        int countChars = 0;

        for (char c : chars) {
            if (Character.isLetter(c)) countChars++;
        }
        System.out.println(countChars);


    }
}

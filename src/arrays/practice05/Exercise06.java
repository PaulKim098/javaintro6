package arrays.practice05;

public class Exercise06 {
    public static void main(String[] args) {
        getDuplicates("baNana");
        getDuplicates("aPple");
    }

    public static void getDuplicates(String str){
        String answer = "";
        for (int i = 0; i < str.length()-1; i++) {     // i = 0 1
            for (int j = i+1; j < str.length(); j++) { // j = 1 2 3 4 5 6; 2 3 4 5 6
                if (str.toLowerCase().charAt(i) == str.toLowerCase().charAt(j) &&
                        !answer.contains("" + str.charAt(i))) {
                    answer += str.charAt(i);
                    break;
                }
            }
        }
        char[] answerArr = answer.toCharArray();
        for (char c : answerArr) {
            System.out.println(c);
        }
    }

}

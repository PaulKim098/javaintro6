package arrays.practice05;


public class Exercise01 {
    public static void main(String[] args) {
        getFirstPosAndNeg();

    }


    public static void getFirstPosAndNeg(){
        System.out.println("\n-----Task-1-----\n");

        int[] numbers = {0, -4, -7, 0, 5, 10, 45};
        int firstPos = 0, firstNeg = 0;

        for (int n : numbers) {
            if(n > 0) {
                firstPos = n;
                break;
            }
        }

        for (int n : numbers) {
            if(n < 0) {
                firstNeg = n;
                break;
            }
        }

        System.out.println("First positive number is: " + firstPos);
        System.out.println("First negative number is: " + firstNeg);

    }









}

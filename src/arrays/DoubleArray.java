package arrays;

import java.util.Arrays;

public class DoubleArray {
    public static void main(String[] args) {

        System.out.println("\n------Task 1-------\n");
        // Create an array to store -> 5.5, 6, 10.3, 25

        double[] doubleNum = {5.5, 6, 10.3, 25};

        System.out.println("\n------Task 2-------\n");
        // Print the array -> [5.5, 6.0, 10.3, 25.0]

        System.out.println(Arrays.toString(doubleNum));

        System.out.println("\n------Task 3-------\n");
        // Print the size of the array -> The length is 4
        System.out.println("The length is " + doubleNum.length);

        System.out.println("\n------Task 4-------\n");
        // Print each element using for each loop

        for (double numbers : doubleNum) {
            System.out.println(numbers);
        }


    }
}

package arrays;

public class Exercise01_CountNumbers {
    public static void main(String[] args) {

        int[] numbers = {-1, 3, 0, 5, -7, 10, 8, 0, 10, 0};

        //Write a program that counts how many negative you have in the array -> 2

        /*
        PSEUDO CODE
        Check each number one by one
        Count Whenever a number is negative
        After checking all numbers, print the result
         */
        System.out.println("\n--------for each loop--------\n");

        int countNeg = 0;

        for (int number : numbers){
            if (number < 0) countNeg++;
        }
        System.out.println(countNeg);


        System.out.println("\n-------for  loop------\n");
        int negatives = 0;

        for (int i = 0; i < numbers.length; i++) {
            if(numbers[i] < 0) negatives++;
        }

        System.out.println(negatives); // 2

        System.out.println("\n-------for each Even------\n");

        //Write a program that counts how many even numbers you have in the array -> 6
        int countEven = 0;

        for (int number : numbers){
            if (number % 2 == 0) countEven++;
        }
        System.out.println(countEven);

        System.out.println("\n-------for each Sum------\n");
        //Write a program that find the sum of all the numbers in the array -> 28

        int sum = 0;

        for (int number : numbers) {
            sum += number;
        }
        System.out.println(sum);

    }
}

package methods;

import utilities.RandomNumberGenerator;

public class Exercise01 {
    public static void main(String[] args) {
        /*
        Create a random number between 5 and 3
        Create a random number between 3 and 4
        Create a random number between 10 and 12


        Find the max of these
         */

        int r1 = RandomNumberGenerator.getRandomNumber(5, 8);
        int r2 = RandomNumberGenerator.getRandomNumber(3, 4);
        int r3 = RandomNumberGenerator.getRandomNumber(10, 12);

        System.out.println(r1);
        System.out.println(r2);
        System.out.println(r3);
    }
}

package methods;

import utilities.ScannerHelper;

public class CheckName {
    public static void main(String[] args) {

        //I can invoke static methods with class name
        //I can call non-static methods with object

        //ScannerHelper scannerHelper = new ScannerHelper();

        String name = ScannerHelper.getFirstName(); // returns a first name
        System.out.println("The name entered by user = " + name);

        String lastName = ScannerHelper.getLastName();
        System.out.println("The full name entered is = " + name + " " + lastName);

        int age = ScannerHelper.getAge();
        System.out.println("The age entered is = " + age);
    }
}

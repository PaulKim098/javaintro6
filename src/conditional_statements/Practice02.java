package conditional_statements;

import java.util.Random;

public class Practice02 {
    public static void main(String[] args) {

        System.out.println("\n-----Task-2------\n");
        /*
        Requirement:
        Write a program that generates a
        random number between 0 and 50
        (both 0 and 50 are included)
        Print true if number is in between 10
        and 25 (10 and 25 included)
        Print false otherwise
         */

        int rand = (int) (Math.random() * 51);
        System.out.println(rand);

        System.out.println(rand >= 10 && rand <= 25 ? "true" : "false");


        System.out.println("\n-----Task-2------\n");

        /*
        Requirement:
        Write a program that generates a random number
        between 1 and 100 (both 1 and 100 are included)
        Find which quarter and half is number in
        1st quarter is 1-25
        2nd quarter is 26-50
        3rd quarter is 51-75
        4th quarter is 76-100
        1st half is 1-50
        2nd half is 51-100
        Test data:
        34
        Expected result:
        34 is in the 1st half
        34 is in the 2nd quarter
         */

        Random random = new Random();
        int rand1 = random.nextInt(100) + 1;

        if (rand1 <= 50) {
            System.out.println(rand1 + " is in the first half");
            if (rand1 <= 25) {
                System.out.println(rand1 + " is in the 1st Quarter");
            } else {
                System.out.println(rand1 + " is in the 2nd Quarter");
            }
        } else {
            System.out.println(rand1 + " is in the 2nd half");
            if (rand1 <= 75) {
                System.out.println(rand1 + " is in the 3rd Quarter");
            } else {
                System.out.println(rand1 + " is in the 4th Quarter");
            }
        }

//        int randomNumber2 = random.nextInt(100) + 1;
//
//        if(randomNumber2 <= 25){
//            System.out.println(randomNumber2 + " is in the 1st half");
//            System.out.println(randomNumber2 + " is in the 1st quarter");
//        } else if(randomNumber2 <= 50){
//            System.out.println(randomNumber2 + " is in the 1st half");
//            System.out.println(randomNumber2 + " is in the 2st quarter");
//        }else if(randomNumber2 <= 75){
//            System.out.println(randomNumber2 + " is in the 2st half");
//            System.out.println(randomNumber2 + " is in the 3rd quarter");
//        } else{
//            System.out.println(randomNumber2 + " is in the 2st half");
//            System.out.println(randomNumber2 + " is in the 4th quarter");
//        }

        /*
        Requirement:
        -Assume you are given a single character. (It will be hard-coded)
        -If given char is a letter, then print “Character is a letter”
        -If given char is a digit, then print “Character is a digit”
        USE ASCII TABLE for this task
        Test data:
        ‘v’
        Expected result:
        Character is a letter

        Test data:
        ‘5’
        Expected result:
        Character is a digit
         */
        char c = 'v';

        if ((c >= 65 && c <= 90) || (c >= 97 && c <= 122)) System.out.println("Character is a letter");
        else if (c >= 48 && c <= 57) System.out.println("Character is a digit");
        else System.out.println("Character is neither a letter or digit");


    }
}

package conditional_statements;

import java.util.Scanner;

public class Exercise04_RetirementAge {
    public static void main(String[] args) {
        /*
        Write a Java program that asks user to enter their age
        And print "It is your time to get retired!" if their age is more than or equals 55
        else calculate and tell user how many years later they can be retired

        EXAMPLE PROGRAM 1
        Program: Please enter your age?
        User: 60
        Program: It is your time to get retired!


        EXAMPLE PROGRAM 1
        Program: Please enter your age?
        User: 45
        Program: You have 10 years to be retired
         */

        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter your age:");
        int userAge = scan.nextInt();

        if (userAge >= 55){
            System.out.println("It is your time to get retired!");
        } else if(userAge == 54){
            System.out.println("You have 1 year left to retire");
        } else{
            System.out.println("You have "+ (55-userAge) + " years to be retired");
        }
    }
}

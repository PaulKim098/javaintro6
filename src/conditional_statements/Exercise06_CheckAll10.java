package conditional_statements;

public class Exercise06_CheckAll10 {
    public static void main(String[] args) {
        /*
        Write a program that generates 2 random numbers between 10 and 11
        If both are 10 -> print true
        Otherwise -> print false
         */

        int rand1 = (int)(Math.random() * 2) + 10;
        int rand2 = (int)(Math.random() * 2) + 10;

        System.out.println(rand1);
        System.out.println(rand2);

        if (rand1 == 10 && rand2 == 10){
            System.out.println(true);
        } else {
            System.out.println(false);
        }
    }
}

package conditional_statements;

import java.util.Scanner;

public class Exercise05_CheckAllEven {
    public static void main(String[] args) {
        /*
        Write a Java program that asks user to enter 3 numbers
        Print true if all of them are even numbers
        Otherwise, print false

        EXAMPLE PROGRAM 1
        Program: Please enter 3 numbers?
        User: 2 4 6
        Program: true


        EXAMPLE PROGRAM 2
        Program: Please enter 3 numbers?
        User: 10 20 25
        Program: false


        EXAMPLE PROGRAM 3
        Program: Please enter 3 numbers?
        User: 1 3 5
        Program: false
         */
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter 3 numbers:");
        int num1 = scan.nextInt();
        int num2 = scan.nextInt();
        int num3 = scan.nextInt();

        if (num1 % 2 == 0 && num2 % 2 == 0 && num3 % 2 == 0){
            System.out.println("true");
        } else {
            System.out.println("false");
        }
    }
}

package loop_practices;

import utilities.ScannerHelper;

public class Exercise04_VowelsInAString {
    public static void main(String[] args) {
        String str = ScannerHelper.getString().toLowerCase();
        int countOfVowels = 0;

        for (int i = 0; i < str.length(); i++) {
            if(str.charAt(i) == 'a' || str.charAt(i) == 'e' || str.charAt(i) == 'i'
            || str.charAt(i) == 'o' || str.charAt(i) == 'u') countOfVowels++;
        }
        System.out.println(countOfVowels);

    }
}

package loop_practices;

import utilities.ScannerHelper;

public class Exercise03_ReachesNumExceptFive {
    public static void main(String[] args) {

        int num1 = ScannerHelper.getNumber();
        int num2 = ScannerHelper.getNumber();

        int start = Math.min(num1, num2);
        int end = Math.max(num1, num2);

        for (int i = start; i <= end ; i++) {
            if(i != 5) System.out.println(i);
        }


    }
}

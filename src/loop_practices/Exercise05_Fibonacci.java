package loop_practices;

public class Exercise05_Fibonacci {
    public static void main(String[] args) {
        int fib = 8;
        int num1 = 0, num2 = 1, sum;
        String ans = "";

        for (int i = 0; i < fib; i++) {
            ans += num1 + " - ";
            sum =  num1 + num2;
            num1 = num2;
            num2 = sum;
        }
        System.out.println(ans.substring(0, ans.length()-3));
    }
}

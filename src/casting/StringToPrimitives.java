package casting;

public class StringToPrimitives {
    public static void main(String[] args) {
        int num1 = 5, num2 = 10;

        System.out.println(num1 + num2); // 15
        System.out.println("" + num1 + num2); // 510
        System.out.println("" + (num1 + num2)); // "15"

        //Converting numbers to String
        System.out.println("" + num1 + num2); // "510"
        System.out.println(num1 + String.valueOf(num2)); // "510"

        System.out.println(String.valueOf(num2 + num1) + (7 + num2)); // "1517"

        //Converting String to primitives
        String price = "1597.06";

        System.out.println(Double.parseDouble(price) - 10.0); // 1587.06
    }
}

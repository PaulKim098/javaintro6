package casting;

public class Exercise04 {
    public static void main(String[] args) {
        String areWeOnline = "true";
        String isEveryoneAJoined = "false";

        // Perfect day when everyone is online participated in
        System.out.println(Boolean.parseBoolean(areWeOnline) && Boolean.parseBoolean(isEveryoneAJoined)); // compiler error

    }
}

package casting;

public class Exercise03 {
    public static void main(String[] args) {
        /*
        Find the multiplication, division, addition, and subtraction of the numbers

        Expected:
        42
        23
        10.5
        19
         */

        String s1 = "21", s2 = "2";
        int num1 = Integer.parseInt(s1);
        int num2 = Integer.parseInt(s2);

        System.out.println(num1 * num2);
        System.out.println(num1 + num2);
        System.out.println((double) num1 / num2);
        System.out.println(num1 - num2);

        System.out.println(Double.parseDouble(s1) * Double.parseDouble(s2));

    }
}

package casting;

public class Exercise02 {
    public static void main(String[] args) {
        /*
        Phone -> $899.99
        $50 per day

        How many days later, you can buy this phone
        18 * 50 -> 900

        Expected Output:
        You can buy the phone after 18 days.
         */

        double price = 900;
        double dailySave = 50;

        System.out.println("You can buy the phone after " + (int) (price / dailySave) + " days.");
    }
}

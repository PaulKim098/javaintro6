package collections;

import java.awt.image.AreaAveragingScaleFilter;
import java.util.*;

public class _02_Set {
    public static void main(String[] args) {
        /*
        Set is an interface, and it has some class implementation as below
            -Set does not keep insertion order
            -Set does not allow duplicates
            -Set allows ONLY 1 null element


        1. HashSet: The most common Set implementation
                - It does not keep insertion order
                - It does not allow duplicates
                - It allows ONLY 1 null element

        2. LinkedList
                - *It does keep insertion order* - predictable
                - It does not allow duplicates
                - It allows ONLY 1 null element

        3. TreeSet
                - It keeps insertion order - it sorts the elements
                - It does not allow duplicates
                - It DOES NOT ALLOW any null element as it does not know how to sort it with numbers and strings
         */

        HashSet<String> objects = new HashSet<>();
        objects.add(null);
        objects.add("Sally");
        objects.add("Johnathan");
        objects.add("Sandina");
        objects.add("Happy");
        objects.add(null);
        objects.add("Okan");
        objects.add("");
        objects.add("Zel");
        objects.add("Alexander");
        objects.add("#@$%2");
        objects.add("Kingston");


        System.out.println(objects); // [null, Sandina, , Zel, Alexander, Johnathan, Sally, Happy, Kingston, Okan, #@$%2]

        objects.remove(null);

        System.out.println(objects); // [Sandina, , Zel, Alexander, Johnathan, Sally, Happy, Kingston, Okan, #@$%2]

        objects.add(null);

        System.out.println(objects); // [Sandina, , Zel, null, Alexander, Johnathan, Sally, Happy, Kingston, Okan, #@$%2]

        ArrayList<String> list = new ArrayList<>(objects);
        System.out.println(list.get(3)); //null

        System.out.println("\n-------LinkedHashSet------\n");
        LinkedHashSet<String> words = new LinkedHashSet<>();
        words.add(null);
        words.add("Sandina");
        words.add(null);
        words.add("Okan");
        words.add("Alex");
        words.add("Alex");
        words.add("John");
        words.add("abc");
        words.add("123");
        words.add("");
        words.add("Sal");
        words.add("Boo");

        System.out.println(words); // [null, Sandina, Okan, Alex, John, abc, 123, , Sal, Boo]

        System.out.println("\n-------TreeSet------\n");
        TreeSet<String> treeSet = new TreeSet<>();
        // treeSet.add(nullPointerException)
        treeSet.add("Sandina");
        treeSet.add("Okan");
        treeSet.add("Alex");
        treeSet.add("Alex");
        treeSet.add("John");
        treeSet.add("abc");
        treeSet.add("123");
        treeSet.add("");
        treeSet.add("Sal");
        treeSet.add("Boo");

        System.out.println(treeSet); // [, 123, Alex, Boo, John, Okan, Sal, Sandina, abc]

        System.out.println(treeSet.descendingSet()); // [abc, Sandina, Sal, Okan, John, Boo, Alex, 123, ]

        System.out.println(treeSet.subSet("Boo", "Sandina")); // [Boo, John, Okan, Sal]

        System.out.println(treeSet.tailSet("Okan")); //  [Okan, Sal, Sandina, abc]
        System.out.println(treeSet.headSet("Okan")); // [, 123, Alex, Boo, John]

        System.out.println(treeSet.floor("Okan")); // Okan
        System.out.println(treeSet.ceiling("Okan")); // Okan

        System.out.println(treeSet.lower("Okan")); // John
        System.out.println(treeSet.higher("Okan")); // Sal

        System.out.println(treeSet.first()); // ""
        System.out.println(treeSet.last()); // abc

        System.out.println("\n---------HashSet, LinkedHashSet & TreeSet in the shape of Set");

        Set<Integer> number1 = new HashSet<>();
        Set<Integer> number2 = new LinkedHashSet<>();
        Set<Integer> number3 = new TreeSet<>();

    }
}

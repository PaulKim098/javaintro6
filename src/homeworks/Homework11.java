package homeworks;

import java.util.Arrays;

public class Homework11 {

    public static void main(String[] args) {
        System.out.println("\n-------Task-01-------");
        System.out.println(noSpace(""));
        System.out.println(noSpace("Java"));
        System.out.println(noSpace("  Hello  "));
        System.out.println(noSpace("Hello World"));
        System.out.println(noSpace("Tech Global"));


        System.out.println("\n-------Task-02-------");
        System.out.println(replaceFirstLast(""));
        System.out.println(replaceFirstLast("A"));
        System.out.println(replaceFirstLast("  A  "));
        System.out.println(replaceFirstLast("Hello"));
        System.out.println(replaceFirstLast("Tech Global"));

        System.out.println("\n-------Task-03-------");
        System.out.println(hasVowel(""));
        System.out.println(hasVowel("Java"));
        System.out.println(hasVowel("1234"));
        System.out.println(hasVowel("ABC"));

        System.out.println("\n-------Task-04-------");
        checkAge(2010);
        checkAge(2006);
        checkAge(2050);
        checkAge(1920);
        checkAge(1800);
        checkAge(1923);

        System.out.println("\n-------Task-05-------");

        System.out.println(averageOfEdges(0, 0, 0));
        System.out.println(averageOfEdges(0, 0, 6));
        System.out.println(averageOfEdges(-2, -2, 10));
        System.out.println(averageOfEdges(-3, 15, -3));
        System.out.println(averageOfEdges(10, 13, 20));

        System.out.println("\n-------Task-06-------");
        System.out.println(Arrays.toString(noA(new String[]{"java", "hello", "123", "xyz"})));
        System.out.println(Arrays.toString(noA(new String[]{"appium", "123", "ABC", "java"})));
        System.out.println(Arrays.toString(noA(new String[]{"apple", "appium", "ABC", "Alex", "A"})));

        System.out.println("\n-------Task-07-------");
        System.out.println(Arrays.toString(no3Or5(new int[]{7, 4, 11, 23, 17})));
        System.out.println(Arrays.toString(no3Or5(new int[]{3, 4, 5, 6})));
        System.out.println(Arrays.toString(no3Or5(new int[]{10, 11, 12, 13, 14, 15})));

        System.out.println("\n-------Task-08-------");
        System.out.println(countPrimes(new int[]{-10, -3, 0, 1}));
        System.out.println(countPrimes(new int[]{7, 4, 11, 23, 17}));
        System.out.println(countPrimes(new int[]{41, 53, 19, 47, 67}));


    }

    public static String noSpace(String str) {

        return str.replaceAll("\\s", "");

    }

    public static String replaceFirstLast(String str) {

        str = str.trim();

        if (str.length() <= 2) return "";

        char first = str.charAt(0);
        char last = str.charAt(str.length() - 1);
        return last + str.substring(1, str.length() - 1) + first;

    }

    public static boolean hasVowel(String str) {

        str = str.toLowerCase();

        return str.contains("a") || str.contains("e") || str.contains("i") || str.contains("o") || str.contains("u");

    }

    public static void checkAge(int yearOfBirth) {

        int age = 2023 - yearOfBirth;

        if (age < 0 || age > 100) System.out.println("AGE IS NOT VALID");
        else if (age < 16) System.out.println("AGE IS NOT ALLOWED");
        else System.out.println("AGE IS ALLOWED");

    }

    public static int averageOfEdges(int num1, int num2, int num3) {

        int max = Math.max(Math.max(num1, num2), num3);
        int min = Math.min(Math.min(num1, num2), num3);

        return (max + min) / 2;

    }

    public static String[] noA(String[] arr) {

        String[] result = new String[arr.length];

        for (int i = 0; i < arr.length; i++) {
            String str = arr[i];
            result[i] = str.toLowerCase().startsWith("a") ? "###" : str;
        }
        return result;

    }

    public static int[] no3Or5(int[] arr) {

        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 15 == 0) result[i] = 101;
            else if (arr[i] % 5 == 0) result[i] = 99;
            else if (arr[i] % 3 == 0) result[i] = 100;
            else result[i] = arr[i];
        }
        return result;

    }

    public static int countPrimes(int[] arr) {

        int count = 0;
        for (int num : arr) {
            boolean isPrime = true;
            if (num <= 1) {
                isPrime = false;
            }
            for (int i = 2; i <= Math.sqrt(num); i++) {
                if (num % i == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                count++;
            }
        }
        return count;
    }
}

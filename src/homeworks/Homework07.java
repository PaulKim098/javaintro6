package homeworks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Homework07 {
    public static void main(String[] args) {

        System.out.println("\n-----Task01-----\n");
        ArrayList<Integer> list1 = new ArrayList<>();
        list1.add(10);
        list1.add(23);
        list1.add(67);
        list1.add(23);
        list1.add(78);

        System.out.println(list1.get(3)); // 23
        System.out.println(list1.get(0)); // 10
        System.out.println(list1.get(2)); // 67
        System.out.println(list1); // [10, 23, 67, 23, 78]

        System.out.println("\n-----Task02-----\n");
        ArrayList<String> colors = new ArrayList<>();

        colors.add("Blue");
        colors.add("Brown");
        colors.add("Red");
        colors.add("White");
        colors.add("Black");
        colors.add("Purple");

        System.out.println(colors.get(1)); // Brown
        System.out.println(colors.get(3)); // White
        System.out.println(colors.get(5)); // Purple
        System.out.println(colors); // [Blue, Brown, Red, White, Black, Purple]

        System.out.println("\n-----Task03-----\n");

        ArrayList<Integer> numbers = new ArrayList<>();

        numbers.add(23);
        numbers.add(-34);
        numbers.add(-56);
        numbers.add(0);
        numbers.add(89);
        numbers.add(100);

        System.out.println(numbers); // [23, -34, -56, 0, 89, 100]
        Collections.sort(numbers);
        System.out.println(numbers); // [-56, -34, 0, 23, 89, 100]

        System.out.println("\n-----Task04-----\n");

        ArrayList<String> cities = new ArrayList<>(Arrays.asList("Istanbul", "Berlin", "Madrid", "Paris"));

        System.out.println(cities); // [Istanbul, Berlin, Madrid, Paris]
        Collections.sort(cities);
        System.out.println(cities); // [Berlin, Istanbul, Madrid, Paris]

        System.out.println("\n-----Task05-----\n");
        ArrayList<String> marvel = new ArrayList<>(Arrays.asList("Spider Man", "Iron Man", "Black Panther",
                 "Deadpool", "Captain America"));

        System.out.println(marvel); // [Spider Man, Iron Man, Black Panther, Deadpool, Captain America]
        System.out.println(marvel.contains("Wolverine")); // false

        System.out.println("\n-----Task06-----\n");

        ArrayList<String> avengers = new ArrayList<>(Arrays.asList("Hulk", "Black Widow", "Captain America", "Iron Man"));

        Collections.sort(avengers);
        System.out.println(avengers);

        ArrayList<String> hulkIronMan = new ArrayList<>(Arrays.asList("Hulk", "Iron Man"));
        System.out.println(avengers.containsAll(hulkIronMan));

        System.out.println("\n-----Task07-----\n");
        ArrayList<Character> chars = new ArrayList<>(Arrays.asList('A', 'x', '$', '%', '9', '*', '+', 'F', 'G'));

        System.out.println(chars);
        chars.forEach(System.out::println);

        System.out.println("\n-----Task08-----\n");
        ArrayList<String> objects = new ArrayList<>(Arrays.asList("Desk", "Laptop", "Mouse", "Monitor", "Mouse-Pad", "Adapter"));

        System.out.println(objects);
        Collections.sort(objects);
        System.out.println(objects);

        int countM = 0;
        int countNotAorE = 0;
        for (String object : objects) {
            if (object.startsWith("M") || object.startsWith("m")) {
                countM++;
            }
            if (!object.toLowerCase().contains("a") && !object.toLowerCase().contains("e")) countNotAorE++;
            }
        System.out.println(countM);
        System.out.println(countNotAorE);

        System.out.println("\n-----Task09-----\n");
        ArrayList<String> kitchenObj = new ArrayList<>(Arrays.asList("Plate", "spoon", "fork", "Knife", "cup", "Mixer"));

        System.out.println(kitchenObj);

        int upper = 0;
        int lower = 0;
        int pP = 0;
        int startOrEndPp = 0;

        for (String s : kitchenObj) {
            if (Character.isUpperCase(s.charAt(0))) upper++;
            else lower++;
            if (s.toLowerCase().contains("p")) {
                pP++;
                if (s.toLowerCase().startsWith("p") || s.toLowerCase().endsWith("p")) startOrEndPp++;
            }
        }
        System.out.println("Elements starts with uppercase = " + upper);
        System.out.println("Elements starts with lowercase = " + lower);
        System.out.println("Elements having P or p = " + pP);
        System.out.println("Elements starting or ending with P or p = " + startOrEndPp);


        System.out.println("\n-----Task10-----\n");
        ArrayList<Integer> numbers1 = new ArrayList<>(Arrays.asList(3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78));

        System.out.println(numbers1);

        int divideBy10 = 0;
        int evenGreater15 = 0;
        int oddLesser20 = 0;
        int less15OrGreater50 = 0;

        for (Integer integer : numbers1) {
            if (integer % 10 == 0) divideBy10++;
            if (integer % 2 == 0 && integer > 15) evenGreater15++;
            if (integer % 2 == 1 && integer < 20) oddLesser20++;
            if (integer < 15 || integer > 50) less15OrGreater50++;
        }
        System.out.println("Elements that can be divided by 10 = " + divideBy10);
        System.out.println("Elements that are even and greater than 15 = " + evenGreater15);
        System.out.println("Elements that are odd and less than 20 = " + oddLesser20);
        System.out.println("Elements that less than 15 or greater than 50 = " + less15OrGreater50);

    }
}

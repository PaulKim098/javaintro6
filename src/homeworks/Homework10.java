package homeworks;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

public class Homework10 {

    public static void main(String[] args) {
        System.out.println("\n-------Task-01-------\n");

        System.out.println(countWords("     Java is fun     "));
        System.out.println(countWords("Selenium is the most common UI automation tool.   "));

        System.out.println("\n-------Task-02-------\n");

        System.out.println(countA("TechGlobal is a QA bootcamp"));
        System.out.println(countA("QA stand for Quality Assurance"));

        System.out.println("\n-------Task-03-------\n");

        System.out.println(countPos(new ArrayList<>(Arrays.asList(-45, 0, 0, 34, 5, 67))));
        System.out.println(countPos(new ArrayList<>(Arrays.asList(-23, -4, 0, 2, 5, 90, 123))));

        System.out.println("\n-------Task-04-------\n");

        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(10, 20, 35, 20, 35, 60, 70, 60))));
        System.out.println(removeDuplicateNumbers(new ArrayList<>(Arrays.asList(1, 2, 5, 2, 3))));

        System.out.println("\n-------Task-05-------\n");

        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("java", "C#", "ruby", "JAVA", "ruby", "C#", "C++"))));
        System.out.println(removeDuplicateElements(new ArrayList<>(Arrays.asList("abc", "xyz", "123", "ab", "abc", "ABC"))));

        System.out.println("\n-------Task-06-------\n");

        System.out.println(removeExtraSpaces("   I   am      learning     Java      "));
        System.out.println(removeExtraSpaces("Java  is fun    "));

        System.out.println("\n-------Task-07-------\n");

        System.out.println(Arrays.toString(add(new int[] {3, 0, 0, 7, 5, 10}, new int[] {6, 3, 2})));
        System.out.println(Arrays.toString(add(new int[] {10, 3, 6, 3, 2}, new int[] {6, 8, 3, 0, 0, 7, 5, 10, 34})));

        System.out.println("\n-------Task-08-------\n");
        System.out.println(findClosestTo10(new int[] {10, -13, 5, 70, 15, 57}));
        System.out.println(findClosestTo10(new int[] {10, -13, 8, 12, 15, -20}));


    }


    // Task - 1
    public static int countWords(String str){
        return str.trim().split(" ").length;
    }

    // Task - 2
    public static int countA(String str){

        int countA = 0;;
        for (char c : str.toCharArray()) {
            if (c == 'a' || c == 'A') countA++;
        }
        return countA;
    }

    // Task - 3
    public static int countPos(ArrayList<Integer> list){
        list.removeIf(num -> num <= 0);

        return list.size();

        /*
        int count = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) > 0) {
                count++;
            }
        }
        return count;
         */
    }

    // Task - 4
    public static ArrayList<Integer> removeDuplicateNumbers(ArrayList<Integer> list){

        ArrayList<Integer> empty = new ArrayList<>();

        for (Integer num : list) {
            if (!empty.contains(num)) empty.add(num);

        }
        return empty;

    }

    // Task - 5
    public static ArrayList<String> removeDuplicateElements(ArrayList<String> list){

        ArrayList<String> empty = new ArrayList<>();

        for (String s : list) {
            if (!empty.contains(s)) empty.add(s);
        }

        return empty;

    }

    // Task - 6
    public static String removeExtraSpaces(String str){

        String newStr = "";


        for (String s : str.trim().split("\\s+")) {

            newStr += s + " ";
        }

        return newStr;
    }

    // Task - 7
    public static int[] add(int[] arr1, int[] arr2){

        int max = Math.max(arr1.length, arr2.length);
        int min = Math.min(arr1.length, arr2.length);

        int[] sum = new int[max];

        for (int i = 0; i < max; i++) {
            if (i < min) sum[i] = arr1[i] + arr2[i];
            else if (arr1.length > arr2.length) sum[i] = arr1[i];
            else sum[i] = arr2[i];
        }

        return sum;

    }


    // Task - 8
    public static int findClosestTo10(int[] arr){

        int closest = Integer.MAX_VALUE;
        int difference = Integer.MAX_VALUE;

        for (int integer : arr) {
            if (integer != 10 && Math.abs(10 - integer) < difference){
                difference = Math.abs(10 - integer);
                closest = integer;
            }
        }

        return closest;
    }

}

package homeworks;

import java.util.Scanner;

public class Homework03 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("\n-------Task-1------\n");
        System.out.println("Please enter 2 numbers:");
        int num1 = input.nextInt();
        int num2 = input.nextInt();

        System.out.println("The difference between numbers is = " + Math.abs(num1 - num2));

        System.out.println("\n-------Task-2------\n");
        System.out.println("Please enter 5 numbers:");
        int numb1 = input.nextInt();
        int numb2 = input.nextInt();
        int numb3 = input.nextInt();
        int numb4 = input.nextInt();
        int numb5 = input.nextInt();

        int max = Math.max(Math.max(Math.max(Math.max(numb1, numb2), numb3), numb4), numb5);
        int min = Math.min(Math.min(Math.min(Math.min(numb1, numb2), numb3), numb4), numb5);

        System.out.println("Max value = " + max);
        System.out.println("Min value = " + min);

        System.out.println("\n-------Task-3------\n");
        int random1 = (int) (Math.random() * 51) + 50;
        int random2 = (int) (Math.random() * 51) + 50;
        int random3 = (int) (Math.random() * 51) + 50;

        System.out.println("Number 1 = " + random1);
        System.out.println("Number 2 = " + random2);
        System.out.println("Number 3 = " + random3);
        System.out.println("The sum of numbers is = " + (random1 + random2 + random3));

        System.out.println("\n-------Task-4------\n");
        double alexBalance = 125;
        double mikeBalance = 220;

        alexBalance -= 25.5;
        mikeBalance += 25.5;

        System.out.println("Alex's money: $" + alexBalance);
        System.out.println("Mike's money: $" + mikeBalance);

        System.out.println("\n-------Task-5------\n");
        double bikeCost = 390;
        double dailySave = 15.60;

        System.out.println((int) (bikeCost / dailySave));

        System.out.println("\n-------Task-6------\n");
        String s1 = "5", s2 = "10";

        System.out.println("Sum of " + s1 + " and " + s2 + " is = " + (Integer.parseInt(s1) + Integer.parseInt(s2)));
        System.out.println("Product of " + s1 + " and " + s2 + " is = " + (Integer.parseInt(s1) * Integer.parseInt(s2)));
        System.out.println("Division of " + s1 + " and " + s2 + " is = " + (Integer.parseInt(s1) / Integer.parseInt(s2)));
        System.out.println("Subtraction of " + s1 + " and " + s2 + " is = " + (Integer.parseInt(s1) - Integer.parseInt(s2)));
        System.out.println("Remainder of " + s1 + " and " + s2 + " is = " + (Integer.parseInt(s1) % Integer.parseInt(s2)));

        System.out.println("\n-------Task-7------\n");
        String s3 = "200", s4 = "-50";

        System.out.println("The greatest value is = " + Math.max(Integer.parseInt(s3), Integer.parseInt(s4)));
        System.out.println("The smallest value is = " + Math.min(Integer.parseInt(s3), Integer.parseInt(s4)));
        System.out.println("The average value is = " + (Integer.parseInt(s3) + Integer.parseInt(s4)) / 2);
        System.out.println("The absolute difference is = " + Math.abs(Integer.parseInt(s3) - Integer.parseInt(s4)));

        System.out.println("\n-------Task-8------\n");
        double quarters = 3 * .25;
        double dime = 1 * .10;
        double nickels = 2 * .05;
        double penny = 1 * .01;

        double dailySave1 = quarters + dime + nickels + penny;

        System.out.println((int) (24 / dailySave1) + " days");
        System.out.println((int) (168 / dailySave1) + " days");
        System.out.println("$" + (dailySave1 * 30) * 5);

        System.out.println("\n-------Task-9------\n");
        double computerCost = 1250;
        double dailySave2 = 62.5;

        System.out.println((int) (computerCost / dailySave2));

        System.out.println("\n-------Task-10------\n");
        double carCost = 14265;
        double option1 = 475.5;
        double option2 = 951;

        System.out.println("Option 1 will take " + (int) (carCost / option1) + " months");
        System.out.println("Option 2 will take " + (int) (carCost / option2) + " months");

        System.out.println("\n-------Task-11------\n");
        System.out.println("Please enter 2 numbers:");
        int number1 = input.nextInt();
        int number2 = input.nextInt();

        System.out.println((double) number1 /  number2);

        System.out.println("\n-------Task-12------\n");
        int rand1 = (int) (Math.random() * 101);
        int rand2 = (int) (Math.random() * 101);
        int rand3 = (int) (Math.random() * 101);

        System.out.println(rand1);
        System.out.println(rand2);
        System.out.println(rand3);

        if (rand1 > 25 && rand2 > 25 && rand3 > 25) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

        System.out.println("\n-------Task-13------\n");
        System.out.println("Please enter a number between 1 to 7:");
        int dayOfTheWeek = input.nextInt();

        switch (dayOfTheWeek) {
            case (1):
                System.out.println("MONDAY");
                break;
            case (2):
                System.out.println("TUESDAY");
                break;
            case (3):
                System.out.println("WEDNESDAY");
                break;
            case (4):
                System.out.println("THURSDAY");
                break;
            case (5):
                System.out.println("FRIDAY");
                break;
            case (6):
                System.out.println("SATURDAY");
                break;
            case (7):
                System.out.println("SUNDAY");
                break;
        }

        System.out.println("\n-------Task-14------\n");

        System.out.println("Tell me your exam results?");
        int result1 = input.nextInt();
        int result2 = input.nextInt();
        int result3 = input.nextInt();

        int average = (result1 + result2 + result3) / 3;

        if (average >= 70) {
            System.out.println("YOU PASSED!");
        } else {
            System.out.println("YOU FAILED!");
        }

        System.out.println("\n-------Task-15------\n");
        System.out.println("Please enter 3 numbers:");
        int match1 = input.nextInt();
        int match2 = input.nextInt();
        int match3 = input.nextInt();

        if (match1 == match2 && match2 == match3 ) {
            System.out.println("TRIPLE MATCH");
        } else if (match1 == match2 || match2 == match3 || match1 == match3) {
            System.out.println("DOUBLE MATCH");
        } else {
            System.out.println("NO MATCH");
        }

//        System.out.println("\n------Task-13------\n");
//        System.out.println("Please enter a number between 1 and 7:");
//
//        int userInput = input.nextInt();
//
//        if (userInput == 1) {
//            System.out.println("MONDAY");
//        } else if (userInput == 2) {
//            System.out.println("TUESDAY");
//        } else if (userInput == 3) {
//            System.out.println("WEDNESDAY");
//        } else if (userInput == 4) {
//            System.out.println("THURSDAY");
//        } else if (userInput == 5) {
//            System.out.println("FRIDAY");
//        } else if (userInput == 6) {
//            System.out.println("SATURDAY");
//        } else {
//            System.out.println("SUNDAY");
//        }
    }
}
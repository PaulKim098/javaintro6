package homeworks;

import java.util.ArrayList;
import java.util.Arrays;

public class Homework09 {

    public static int firstDuplicatedNumber(int[] arr){

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j]) return arr[j];
            }
        }
        return -1;

//        Arrays.sort(arr);
//        for(int i = 0; i < arr.length - 1; i++) {
//            if (arr[i] == arr[i + 1]) {
//                return arr[i + 1];
//            }
//        }
//        return -1;
    }

    public static String firstDuplicatedString(String[] str){

        for (int i = 0; i < str.length - 1; i++) {
            for (int j = i + 1; j < str.length; j++) {
                if ((str[i].equalsIgnoreCase(str[j]))) return str[i];
            }
        }
        return "There is no duplicates";
    }

    public static ArrayList<Integer> duplicatedNumbers(int[] arr){

        ArrayList<Integer> list = new ArrayList<>();

        for (int i = 0; i < arr.length - 1; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                if (arr[i] == arr[j] && !(list.contains(arr[i]))) list.add(arr[i]);
            }
        }
        return list;

    }

    public static ArrayList<String> duplicatedStrings(String[] arr){

//        ArrayList<String> list = new ArrayList<>();
//        String empty = "";
//
//        for (int i = 0; i < arr.length - 1; i++) {
//            for (int j = i + 1; j < arr.length; j++) {
//                if (arr[i].equalsIgnoreCase(arr[j]) && !(empty.toLowerCase().contains(arr[i].toLowerCase()))) {
//                    empty += arr[i] + " ";
//                }
//            }
//        }
//        Collections.addAll(list, empty.split(" "));
//        return list;

        ArrayList<String> list = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
            String current = arr[i];
            boolean isDuplicate = false;

            for (int j = i + 1; j < arr.length; j++) {
                if (current.equalsIgnoreCase(arr[j])) {
                    isDuplicate = true;
                    break;
                }
            }

            if (isDuplicate && !list.contains(current)) {
                list.add(current);
            } else if (!isDuplicate && list.contains(current)) {
                list.remove(current);
            }
        }
        return list;
    }

    public static String[] reversedArray(String[] arr){

        String[] reverse = new String[arr.length];

        int o = 0;

        for (int i = arr.length-1; i >= 0; i--) {
            reverse[o++] = arr[i];
        }
        return reverse;

    }

    public static String reverseStringWords(String str){

        String reverse = "";

        for (String e : str.split(" ")) {
            for (int i = e.length()-1; i >= 0 ; i--) {
                reverse += e.charAt(i);
            }
            reverse += " ";
        }
        return reverse;
    }


    public static void main(String[] args) {
        System.out.println("\n--------Task01--------\n");
        System.out.println(firstDuplicatedNumber(new int[] {-4, 0, -7, 0, 5, 10, 45, 45}));
        System.out.println(firstDuplicatedNumber(new int[] {-8, 56, 7, 8, 65}));
        System.out.println(firstDuplicatedNumber(new int[] {3, 4, 3, 3, 5, 5, 6, 6, 7}));

        System.out.println("\n--------Task02--------\n");
        System.out.println(firstDuplicatedString(new String[] {"Z", "abc", "z", "123", "#"}));
        System.out.println(firstDuplicatedString(new String[] {"xyz", "java", "abc"}));
        System.out.println(firstDuplicatedString(new String[] {"a", "b", "B", "XYZ", "123"}));

        System.out.println("\n--------Task03--------\n");
        System.out.println(duplicatedNumbers(new int[] {0, -4, -7, 0, 5, 10, 45, -7, 0}));
        System.out.println(duplicatedNumbers(new int[] {1, 2, 5, 0, 7}));

        System.out.println("\n--------Task04--------\n");
        System.out.println(duplicatedStrings(new String[] {"A", "foo", "12" , "Foo", "bar", "a", "a", "java"}));
        System.out.println(duplicatedStrings(new String[] {"python", "foo", "bar", "java", "123"}));

        System.out.println("\n--------Task05--------\n");
        System.out.println(Arrays.toString(reversedArray(new String[]{"abc", "foo", "bar"})));
        System.out.println(Arrays.toString(reversedArray(new String[]{"java", "python", "ruby"})));

        System.out.println("\n--------Task06--------\n");
        System.out.println(reverseStringWords("Java is fun"));
        System.out.println(reverseStringWords("Today is a fun day"));

    }
}

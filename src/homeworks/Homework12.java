package homeworks;

import java.util.Arrays;

public class Homework12 {

    public static void main(String[] args) {
        System.out.println("\n-----Task 01------\n");
        System.out.println(noDigit(""));
        System.out.println(noDigit("Java"));
        System.out.println(noDigit("123Hello"));
        System.out.println(noDigit("123Hello World149"));
        System.out.println(noDigit("123Tech456Global149"));

        System.out.println("\n-----Task 02------\n");
        System.out.println(noVowel(""));
        System.out.println(noVowel("xyz"));
        System.out.println(noVowel("JAVA"));
        System.out.println(noVowel("125$"));
        System.out.println(noVowel("TechGlobal"));

        System.out.println("\n-----Task 03------\n");
        System.out.println(sumOfDigits(""));
        System.out.println(sumOfDigits("Java"));
        System.out.println(sumOfDigits("John's age is 29"));
        System.out.println(sumOfDigits("$125.0"));

        System.out.println("\n-----Task 04------\n");
        System.out.println(hasUpperCase(""));
        System.out.println(hasUpperCase("java"));
        System.out.println(hasUpperCase("John's age is 29"));
        System.out.println(hasUpperCase("$125.0"));

        System.out.println("\n-----Task 05------\n");
        System.out.println(middleInt(1, 1, 1));
        System.out.println(middleInt(1, 2, 2));
        System.out.println(middleInt(5, 5, 8));
        System.out.println(middleInt(-1, 25, 10));

        System.out.println("\n-----Task 06------\n");
        System.out.println(Arrays.toString(no13(new int[]{1, 2, 3, 4})));
        System.out.println(Arrays.toString(no13(new int[]{13, 2, 3})));
        System.out.println(Arrays.toString(no13(new int[]{13, 13, 13, 13})));

        System.out.println("\n-----Task 07------\n");
        System.out.println(Arrays.toString(arrFactorial(new int[] {1, 2, 3, 4})));
        System.out.println(Arrays.toString(arrFactorial(new int[] {0, 5})));
        System.out.println(Arrays.toString(arrFactorial(new int[] {5, 0, 6})));
        System.out.println(Arrays.toString(arrFactorial(new int[] {})));

        System.out.println("\n-----Task 08------\n");
        System.out.println(Arrays.toString(categorizeCharacters("   ")));
        System.out.println(Arrays.toString(categorizeCharacters("abc123$#%")));
        System.out.println(Arrays.toString(categorizeCharacters("12ab$%3c%")));

    }


    public static String noDigit(String str){
        if(str == null || str.isEmpty()) return str;
        
        StringBuilder result = new StringBuilder();
        for (char c : str.toCharArray()) {
            if(!Character.isDigit(c)) result.append(c);
        }
        return result.toString();
    }

    public static String noVowel(String str){
        if(str == null || str.isEmpty()) return str;

        StringBuilder result = new StringBuilder();
        for (char c : str.toCharArray()) {
            if(!isVowel(c)) result.append(c);
        }
        return result.toString();
    }

    public static boolean isVowel(char c){
        c = Character.toLowerCase(c);

        return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u';
    }

    public static int sumOfDigits(String str){
        int sum = 0;

        for (char c : str.toCharArray()) {
            if(Character.isDigit(c)) sum += Character.getNumericValue(c);
        }
        return sum;
    }

    public static boolean hasUpperCase(String str){
        for (char c : str.toCharArray()) {
            if(Character.isUpperCase(c)) return true;
        }
        return false;
    }

    public static int middleInt(int num1, int num2, int num3){
        int max = Math.max(Math.max(num1, num2), num3);
        int min = Math.min(Math.min(num1, num2), num3);

        return num1 + num2 + num3 - max - min;
    }

    public static int[] no13(int[] arr){
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == 13) result[i] = 0;
            else result[i] = arr[i];
        }
        return result;
    }

    public static int[] arrFactorial(int[] arr) {
        int[] result = new int[arr.length];

        for (int i = 0; i < arr.length; i++) {
            int num = arr[i];
            int factorial = 1;

            if(num > 0){
                for (int j = 1; j <= num; j++) {
                    factorial *= j;
                }
            }
            result[i] = factorial;
        }
        return result;
    }

    public static String[] categorizeCharacters(String str){
        StringBuilder letters = new StringBuilder();
        StringBuilder digits = new StringBuilder();
        StringBuilder specials = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if(Character.isLetter(c)) letters.append(c);
            else if (Character.isDigit(c)) digits.append(c);
            else if (!Character.isLetterOrDigit(c)) specials.append(c);
        }

        String[] result = {letters.toString(), digits.toString(), specials.toString()};
        return result;
    }
}

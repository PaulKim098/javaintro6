package homeworks;

import utilities.ScannerHelper;

public class Homework05 {
    public static void main(String[] args) {
        System.out.println("---------Task-1---------\n");

        for (int i = 1; i <= 100; i++) {
            if (i % 7 == 0) {
                System.out.print(i);
                if (i != 98) {
                    System.out.print(" - ");
                }
            }
        }
        System.out.println("");

        System.out.println("\n---------Task-2---------\n");

        for (int i = 1; i <= 50; i++) {
            if (i % 6 == 0) {
                System.out.print(i);
                if (i != 48) {
                    System.out.print(" - ");
                }
            }
        }
        System.out.println("");

        System.out.println("\n---------Task-3---------\n");

//        for (int i = 100; i >= 50 ; i--) {
//            if (i % 5 == 0) {
//                System.out.print(i);
//                if (i != 50){
//                    System.out.print(" - ");
//                }
//            }
//        }

        // Better way

        String solution = "";
        for (int i = 100; i >= 50; i--) {
            if(i % 5 == 0) solution+= i + " - ";
        }
        System.out.println(solution.substring(0, solution.length() - 3));


        System.out.println("");

        System.out.println("\n---------Task-4---------\n");
        for (int i = 0; i <= 7 ; i++) {
            System.out.println("The square of " + i + " is = " + (i*i));
        }

        System.out.println("");

        System.out.println("\n---------Task-5---------\n");
        int sum = 0;

        for (int i = 1; i <= 10 ; i++) {
            sum += i;
        }
        System.out.println(sum);

        System.out.println("");

        System.out.println("\n---------Task-6---------\n");
        int num;

        do{
            System.out.println("Enter only a positive number.");
            num = ScannerHelper.getNumber();
        } while (num < 0);

        int factorial = 1;
        for (int i = 1; i <= num ; i++) {
            factorial *= i;
        }

        System.out.println(num + "! = " + factorial);

        /*
        int number = ScannerHelper.getNumber(); //5
        int answer - 1; //1 1 2

        for ( int i = 1; i <= number; i++){
            answer *= answer; //answer = answer * i;
        }
        System.out.println(answer);
         */

        System.out.println("");

        System.out.println("\n---------Task-7---------\n");

        String fullName = ScannerHelper.getFullName().toLowerCase();
        int countOfVowels = 0;

        for (int i = 0; i <= fullName.length()-1; i++) {
            if (fullName.charAt(i) == 'a' || fullName.charAt(i) == 'e'
                    || fullName.charAt(i) == 'i' || fullName.charAt(i) == 'o'
            || fullName.charAt(i) == 'u') countOfVowels++;
        }
        System.out.println("There are " + countOfVowels + " vowel letters in this full name");

        System.out.println("");

        System.out.println("\n---------Task-8---------\n");
        String name;

        do{
            name = ScannerHelper.getFirstName().toLowerCase();
        } while (!name.startsWith("j"));


        System.out.println("End of the program");

    }
}

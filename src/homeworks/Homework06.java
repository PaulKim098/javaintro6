package homeworks;

import java.util.Arrays;

public class Homework06 {
    public static void main(String[] args) {

        System.out.println("\n-------Task 1-------\n");
        int[] numbers = new int[10];
        numbers[2] = 23;
        numbers[4] = 12;
        numbers[7] = 34;
        numbers[9] = 7;
        numbers[6] = 15;
        numbers[0] = 89;

        System.out.println((numbers[3])); //0
        System.out.println((numbers[0])); //89
        System.out.println((numbers[9])); //7
        System.out.println(Arrays.toString(numbers)); //[89, 0, 23, 0, 12, 0, 15, 34, 0, 7]

        System.out.println("\n-------Task 2-------\n");
        String[] str = new String[5];

        str[1] = "abc";
        str[4] = "xyz";

        System.out.println(str[3]); //null
        System.out.println(str[0]); //null
        System.out.println(str[4]); //xyz
        System.out.println(Arrays.toString(str)); //[null, abc, null, null, xyz]

        System.out.println("\n-------Task 3-------\n");
        numbers = new int[]{23, -34, -56, 0, 89, 100};

        System.out.println(Arrays.toString(numbers));
        Arrays.sort(numbers);
        System.out.println(Arrays.toString(numbers));

        System.out.println("\n-------Task 4-------\n");
        String[] countries = {"Germany", "Argentina", "Ukraine", "Romania"};

        System.out.println(Arrays.toString(countries)); //[Germany, Argentina, Ukraine, Romania]
        Arrays.sort(countries);
        System.out.println(Arrays.toString(countries)); //[Argentina, Germany, Romania, Ukraine]

        System.out.println("\n-------Task 5-------\n");
        String[] cartoonDogs = {"Scooby Doo", "Snoopy", "Blue", "Pluto", "Dino", "Sparky"};

        System.out.println(Arrays.toString(cartoonDogs)); //[Scooby Doo, Snoopy, Blue, Pluto, Dino, Sparky]

        boolean hasPluto = false;

        for (String cartoonDog : cartoonDogs) {
            if (cartoonDog.contains("Pluto")) {
                hasPluto = true;
            }
        }
        System.out.println(hasPluto); //true

        System.out.println("\n-------Task 6-------\n");
        String[] cartoonCats = {"Garfield", "Tom", "Sylvester", "Azrael"};
        Arrays.sort(cartoonCats);

        System.out.println(Arrays.toString(cartoonCats)); //[Azrael, Garfield, Sylvester, Tom]

        boolean hasGarfieldOrFelix = false;

        for (String cartoonCat : cartoonCats) {
            if (cartoonCat.contains("Garfield") || cartoonCat.contains("Felix")){
                hasGarfieldOrFelix = true;
            }
        }
        System.out.println(hasGarfieldOrFelix); //true

        System.out.println("\n-------Task 7-------\n");
        double[] doubleNum = {10.5, 20.75, 70, 80, 15.75};

        System.out.println(Arrays.toString(doubleNum)); //[10.5, 20.75, 70.0, 80.0, 15.75]

        for (double v : doubleNum) {
            System.out.println(v);
        }

        System.out.println("\n-------Task 8-------\n");
        char[] characters = {'A', 'b', 'G', 'H', '7', '5', '&', '*', 'e', '@', '4'};

        System.out.println(Arrays.toString(characters)); //[A, b, G, H, 7, 5, &, *, e, @, 4]
        int countLetters = 0;
        int countUpper = 0;
        int countLower = 0;
        int countDigits = 0;
        int countSpecials = 0;

        for (char character : characters) {
            if (Character.isLetter(character)) countLetters++;
            if (Character.isUpperCase(character)) countUpper++;
            if (Character.isLowerCase(character)) countLower++;
            if (Character.isDigit(character)) countDigits++;
            if (!Character.isLetterOrDigit(character)) countSpecials++;
        }
        System.out.println("Letters = " + countLetters); //Letters = 5
        System.out.println("Uppercase letters = " + countUpper); //Uppercase letters = 3
        System.out.println("Lowercase letters = " + countLower); //Lowercase letters = 2
        System.out.println("Digits = " + countDigits); //Digits = 3
        System.out.println("Special characters = " + countSpecials); //Special characters = 3

        System.out.println("\n-------Task 9-------\n");
        String[] objects = {"Pen", "notebook", "Book", "paper", "bag", "pencil", "Ruler"};

        System.out.println(Arrays.toString(objects)); //[Pen, notebook, Book, paper, bag, pencil, Ruler]
        countUpper = 0;
        countLower = 0;
        int countBP = 0;
        int countBookPen = 0;

        for (String object : objects) {
            if (Character.isUpperCase(object.charAt(0))) countUpper++;
            else countLower++;
            if (object.toLowerCase().startsWith("b") || object.toLowerCase().startsWith("p")) countBP++;
            if (object.toLowerCase().contains("book") || object.toLowerCase().contains("pen")) countBookPen++;
        }

        System.out.println("Elements starts with uppercase = " + countUpper); //Elements starts with uppercase = 3
        System.out.println("Elements starts with lowercase = " + countLower); //Elements starts with lowercase = 4
        System.out.println("Elements starting with B or P = " + countBP); //Elements starting with B or P = 5
        System.out.println("Elements having \"book\" or \"pen\" = " + countBookPen); //Elements having "book" or "pen" = 4

        System.out.println("\n-------Task 10-------\n");
        numbers = new int[]{3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78};

        int countMore10 = 0;
        int countLess10 = 0;
        int count10 = 0;

        System.out.println(Arrays.toString(numbers)); //[3, 5, 7, 10, 0, 20, 17, 10, 23, 56, 78]
        for (int number : numbers) {
            if (number > 10) countMore10++;
            else if (number < 10) countLess10++;
            else if (number == 10) count10++;
        }
        System.out.println("Elements that are more than 10 = " + countMore10); //Elements that are more than 10 = 5
        System.out.println("Elements that are less than 10 = " + countLess10); //Elements that are less than 10 = 4
        System.out.println("Elements that are 10 = " + count10); //Elements that are 10 = 2

        System.out.println("\n-------Task 11-------\n");
        int[] numbers1 = {5, 8, 13, 1, 2};
        int[] numbers2 = {9, 3, 67, 1, 0};
        int[] numbers3 = new int[5];

        for (int i = 0; i < numbers1.length; i++) {
            numbers3[i] += Math.max(numbers1[i], numbers2[i]);
        }

        System.out.println("1st array is = " + Arrays.toString(numbers1)); //1st array is = [5, 8, 13, 1, 2]
        System.out.println("2nd array is = " + Arrays.toString(numbers2)); //2nd array is = [9, 3, 67, 1, 0]
        System.out.println("3rd array is = " + Arrays.toString(numbers3)); //3rd array is = [9, 8, 67, 1, 2]

    }
}

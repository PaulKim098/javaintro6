package homeworks;

import java.util.Scanner;

public class Homework02 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("\n-------Task-1------\n");
        System.out.println("Please enter 2 numbers:");
        int num1 = scan.nextInt();
        int num2 = scan.nextInt();
        int sum = num1 + num2;

        System.out.println("The number 1 entered by user is = " + num1);
        System.out.println("The number 2 entered by user is = " + num2);
        System.out.println("The sum of number 1 and 2 entered by user is = " + sum);

        System.out.println("\n-------Task-2------\n");
        System.out.println("Please enter 2 numbers:");
        int input1 = scan.nextInt();
        int input2 = scan.nextInt();

        System.out.println("The product of the given 2 numbers is: " + (input1 * input2));

        System.out.println("\n-------Task-3------\n");
        System.out.println("Please enter 2 floating numbers:");
        double userInput1 = scan.nextDouble();
        double userInput2 = scan.nextDouble();

        System.out.println("The sum of the given numbers is = " + (userInput1 + userInput2));
        System.out.println("The product of the given numbers is = " + (userInput1 * userInput2));
        System.out.println("The subtraction of the given numbers is = " + (userInput1 - userInput2));
        System.out.println("The division of the given numbers is = " + (userInput1 / userInput2));
        System.out.println("The remainder of the given numbers is = " + (userInput1 % userInput2));

        System.out.println("\n-------Task-4------\n");
        System.out.println(-10 + 7 * 5); // 25
        System.out.println((72 + 5) % 24); // 0
        System.out.println(10 + -3 * 9 / 9); // 7
        System.out.println(5 + 18 / 3 * 3 - (6 % 3)); // 23

        System.out.println("\n-------Task-5------\n");
        System.out.println("Please enter 2 numbers:");
        int number1 = scan.nextInt();
        int number2 = scan.nextInt();

        System.out.println("The average of the given numbers is: " + ((number1 + number2) / 2));

        System.out.println("\n-------Task-6------\n");
        System.out.println("Please enter 5 numbers:");
        int userNum1 = scan.nextInt();
        int userNum2 = scan.nextInt();
        int userNum3 = scan.nextInt();
        int userNum4 = scan.nextInt();
        int userNum5 = scan.nextInt();

        System.out.println("The average of the given numbers is: " + ((userNum1 + userNum2 + userNum3 + userNum4 +
                userNum5) / 5));

        System.out.println("\n-------Task-7------\n");
        System.out.println("Please enter 3 numbers:");
        int numb1 = scan.nextInt();
        int numb2 = scan.nextInt();
        int numb3 = scan.nextInt();

        System.out.println("The " + numb1 + " multiplied with " + numb1 + " is = " + (numb1 * numb1));
        System.out.println("The " + numb2 + " multiplied with " + numb2 + " is = " + (numb2 * numb2));
        System.out.println("The " + numb3 + " multiplied with " + numb3 + " is = " + (numb3 * numb3));

        System.out.println("\n-------Task-8------\n");
        System.out.println("Please enter a length of the side of a square:");
        int side = scan.nextInt();

        System.out.println("Perimeter of the square = " + (4 * side));
        System.out.println("Area of the square = " + (side * side));

        System.out.println("\n-------Task-9------\n");
        double sDETSalary = 90_000;
        double threeYears = sDETSalary * 3;

        System.out.println("A Software Engineer in Test can earn $" + threeYears + " in 3 years.");

        System.out.println("\n-------Task-10------\n");
        scan.nextLine();
        System.out.println("Please enter your favorite book:");
        String favBook = scan.nextLine();

        System.out.println("Please enter your favorite color:");
        String favColor = scan.nextLine();

        System.out.println("Please enter your favorite number:");
        int favNumber = scan.nextInt();

        System.out.println("User's favorite book is: " + favBook);
        System.out.println("User's favorite color is: " + favColor);
        System.out.println("User's favorite number is: " + favNumber);

        System.out.println("\n-------Task-11------\n");
        scan.nextLine();

        System.out.println("Please enter your first name:");
        String firstName = scan.nextLine();

        System.out.println("Please enter your last name:");
        String lastName = scan.nextLine();

        System.out.println("Please enter your age:");
        int age = scan.nextInt();
        scan.nextLine();

        System.out.println("Please enter your email address:");
        String email = scan.nextLine();

        System.out.println("Please enter your phone number:");
        String phoneNum = scan.nextLine();

        System.out.println("Please enter your address:");
        String address = scan.nextLine();

        System.out.println("\tUser who joined this program is " + firstName + " " + lastName + ". " + firstName + "'s age is "
        + age + ". " + firstName + "'s email \naddress is " + email + ", phone number is " + phoneNum + ", and address \nis " +
                address + ".");

    }
}

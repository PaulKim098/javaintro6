package homeworks;

public class Homework01 {
    public static void main(String[] args) {
        System.out.println("\n--------TASK-1--------\n");
        /*
        Find the binary representation of below words

                      128 64 32 16 8 4 2 1
        J = 74  ->     0  1  0  0  1 0 1 0  (64 + 8 + 2 = 74)
        A = 65  ->     0  1  0  0  0 0 0 1  (64 + 1 = 65)
        V = 86  ->     0  1  0  1  0 1 1 0  (64 + 16 + 4 + 2 = 86)
        A = 65  ->     0  1  0  0  0 0 0 1  (64 + 1 = 65)
        Java    -> 01001010 01000001 01010110 01000001

                      128 64 32 16 8 4 2 1
        S = 83  ->     0  1  0  1  0 0 1 1  (64 + 16 + 2 + 1 = 83)
        E = 69  ->     0  1  0  0  0 1 0 1  (64 + 4 + 1 = 69)
        L = 76  ->     0  1  0  0  1 1 0 0  (64 + 8 + 4 = 76)
        E = 69  ->     0  1  0  0  0 1 0 1  (64 + 4 + 1 = 69)
        N = 78  ->     0  1  0  0  1 1 1 0  (64 + 8 + 4 + 2 = 78)
        I = 73  ->     0  1  0  0  1 0 0 1  (64 + 8 + 1 = 73)
        U = 85  ->     0  1  0  1  0 1 0 1  (64 + 16 + 4 + 1 = 85)
        M = 77  ->     0  1  0  0  1 1 0 1  (64 + 8 + 4 + 1 = 77)
        Selenium -> 01010011 01000101 01001100 01000101 01001110 01001001 01010101 01001101
         */

        System.out.println("\n---------TASK2---------\n");
        /*
        01001101 = 64 + 8 + 4 + 1 = 77 ->         M
        01101000 = 64 + 32 + 8 = 104 ->           h
        01111001 = 64 + 32 + 16 + 8 + 1 = 121 ->  y
        01010011 = 64 + 16 + 2 + 1 = 83 ->        S
        01101100 = 64 + 32 + 8 + 4 = 108 ->       l
         */

        System.out.println("\n---------TASK3---------\n");

        System.out.println("I start to practice \"JAVA\" today, and I like it.");
        System.out.println("The secret of getting ahead is getting started.");
        System.out.println("\"Don't limit yourself. \"");
        System.out.println("Invest in your dreams. Grind now. Shine later.");
        System.out.println("It’s not the load that breaks you down, it’s the way you carry it");
        System.out.println("The hard days are what make you stronger.");
        System.out.println("You can waste your lives drawing lines. Or you can live your life crossing them");

        System.out.println("\n---------TASK4---------\n");
        System.out.println("\tJava is easy to write and easy to run—this is the foundational \n" +
                        "strength of Java and why many developers program in it. When you\n" +
                        "write Java once, you can run it almost anywhere at any time.\n\n\t" +
                        "Java can be used to create complete applications that can run on\n" +
                        "a single computer or be distributed across servers and clients in a \n" +
                        "network.\n\n\tAs a result, you can use it to easily build mobile applications or \n" +
                        "run-on desktop applications that use different operating systems and\n" +
                        "servers, such as Linux or Windows.");

        System.out.println("\n---------TASK5---------\n");

        int myAge = 24;
        System.out.println(myAge);

        int myFavoriteNumber = 64;
        System.out.println(myFavoriteNumber);

        double myHeight = 174.5; // in cm
        System.out.println(myHeight);

        double myWeight = 178.2;
        System.out.println(myWeight);

        char myFavoriteLetter = 'P';
        System.out.println(myFavoriteLetter);

    }
}

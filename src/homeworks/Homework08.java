package homeworks;

import utilities.ScannerHelper;

import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Homework08 {
    public static void main(String[] args) {
        System.out.println("\n------Task-1------\n");
        System.out.println(countConsonants("hello"));
        System.out.println(countConsonants("JAVA"));
        System.out.println(countConsonants(""));

        System.out.println("\n------Task-2------\n");
        System.out.println(Arrays.toString(wordArray("hello")));
        System.out.println(Arrays.toString(wordArray("java is  fun")));
        System.out.println(Arrays.toString(wordArray("Hello, nice to meet you!!")));

        System.out.println("\n------Task-3------\n");
        System.out.println(removeExtraSpaces("hello"));
        System.out.println(removeExtraSpaces("java is  fun"));
        System.out.println(removeExtraSpaces("Hello,  nice to  meet  you!!"));

        System.out.println("\n------Task-4------\n");
        System.out.println(count3OrLess());
        System.out.println(count3OrLess());
        System.out.println(count3OrLess());

        System.out.println("\n------Task-5------\n");
        System.out.println(isDateFormatValid("01/21/1999"));
        System.out.println(isDateFormatValid("1/20/1991"));
        System.out.println(isDateFormatValid("10/2/1991"));
        System.out.println(isDateFormatValid("12-20-2000"));
        System.out.println(isDateFormatValid("12/16/19500"));

        System.out.println("\n------Task-6------\n");
        System.out.println(isEmailFormatValid("abc@gmail.com"));
        System.out.println(isEmailFormatValid("abc@student.techglobal.com"));
        System.out.println(isEmailFormatValid("a@gmail.com"));
        System.out.println(isEmailFormatValid("abcd@@gmail.com"));
        System.out.println(isEmailFormatValid("abc@gmail"));


    }

    public static int countConsonants(String str){
        str = str.replaceAll("[aeiouAEIOU]", "");
        return str.length();
    }

    public static String[] wordArray(String str){

        return str.split("[\\s!]+");
    }

    public static String removeExtraSpaces(String str){

        return str.replaceAll("[\\s+]$", "").replaceAll("\\s+", " ");
    }

    public static int count3OrLess(){
        String str1 = ScannerHelper.getSentence();

        int count = 0;

        Matcher matcher = Pattern.compile("\\b\\w{1,3}\\b").matcher(str1);
        while (matcher.find()){
            count++;
        }
        return count;
    }

    public static boolean isDateFormatValid(String dateOfBirth){

        return  dateOfBirth.matches("\\d{2}/\\d{2}/\\d{4}");

    }

    public static boolean isEmailFormatValid(String email){

        return email.matches("[\\w.#$-]{2,}@[\\w.]{2,}\\.[\\w]{2,}");
    }



}

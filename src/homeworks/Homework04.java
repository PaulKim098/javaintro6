package homeworks;

import utilities.ScannerHelper;

public class Homework04 {
    public static void main(String[] args) {

        System.out.println("--------Task-1--------");
        String firstName = ScannerHelper.getFirstName();

        System.out.println("The length of the name is = " + firstName.length());
        System.out.println("The first character in the name is = " + firstName.charAt(0));
        System.out.println("The last character in the name is = " + firstName.charAt(firstName.length()-1));
        System.out.println("The first 3 characters in the name are = " + firstName.substring(0, 3));
        System.out.println("The last 3 characters in the name are = " + firstName.substring(firstName.length()-3));

        if (firstName.charAt(0) == 'A' || firstName.charAt(0) == 'a') System.out.println("You are in the club!");
        else System.out.println("Sorry, you are not in the club");

        System.out.println("--------Task-2--------");
        String address = ScannerHelper.getAddress();

        if (address.toLowerCase().contains("chicago")) System.out.println("You are in the club");
        else if (address.toLowerCase().contains("des plains")) System.out.println("You are welcome to join to the club");
        else System.out.println("Sorry, you will never be in the club");

        System.out.println("--------Task-3--------");
        String favCountry = ScannerHelper.getFavCountry().toUpperCase();

        if (favCountry.contains("A") || favCountry.contains("I")) {
            if (favCountry.contains("A") && favCountry.contains("I")) System.out.println("A and i are there");
            else if (favCountry.contains("A")) System.out.println("A is there");
            else if (favCountry.contains("I")) System.out.println("I is there");
        }else System.out.println("A and i are not there");

        System.out.println("--------Task-4--------");
        String str = ("   Java is FUN   ");

        str = str.trim();

        String str1 = str.toLowerCase().substring(0,4);
        String str2 = str.toLowerCase().substring(5,7);
        String str3 = str.toLowerCase().substring(8);

        System.out.println("The first world in the str is = " + str1);
        System.out.println("The second world in the str is = " + str2);
        System.out.println("The third world in the str is = " + str3);
    }
}

package character_class;

import utilities.ScannerHelper;

public class UnderstandingCharacterClass {
    public static void main(String[] args) {

        /*
        char is primitive data type
        Character is a wrapper class
        Character is object representation char primitive

        Wrapper classes provides us with some methods that allows us to manipulate the data
         */

        String str = ScannerHelper.getString();

        //Print true if str starts with uppercase, print false otherwise

        if (str.charAt(0) >= 65 && str.charAt(0) <= 90) System.out.println(true);
        else System.out.println(false);
    }
}

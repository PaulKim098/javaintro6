package character_class;

import utilities.ScannerHelper;

public class CountCharacters {
    public static void main(String[] args) {
        System.out.println("\n--------Task-01--------\n");
        /*
        Write a method that asks user to enter a String
        Count letters
        Count digits

        Examples
        "123 Street Chicago"    -> This String has 3 digits and 13 letters
        "ABC"                   -> This String has 0 digits and 3 letters
        "12345"                 -> This String has 5 digits and 0 letters
        "   "                   -> This String has 0 digits and 0 letters
         */

        String str = ScannerHelper.getString();

        int countL = 0;
        int countD = 0;

        for (int i = 0; i <= str.length() - 1; i++) {
            if (Character.isLetter(str.charAt(i))) countL++;
            else if (Character.isDigit(str.charAt(i))) countD++;
        }
        System.out.println("This string has " + countD + " digits and " + countL + " letters.");

        System.out.println("\n--------Task-02--------\n");
        /*
        Write a method that asks user to enter a String
        Uppercase letters
        Lowercase letters

        Examples
        "123 Street Chicago"    -> This string has 2 uppercase letters and 11 lowercase letters
        "ABC"                   -> This string has 3 uppercase letters and 0 lowercase letters
        "12345"                 -> This string has 0 uppercase letters and 0 lowercase letters
        "     "                 -> This string has 0 uppercase letters and 0 lowercase letters

         */

        str = ScannerHelper.getString();

        int countUpper = 0;
        int countLower = 0;

        for (int i = 0; i <= str.length() - 1; i++) {
            if (Character.isUpperCase(str.charAt(i))) countUpper++;
            else if (Character.isLowerCase(str.charAt(i))) countLower++;
        }
        System.out.println("This string has " + countUpper + " uppercase letters and " + countLower + " lowercase letters");


        System.out.println("\n--------Task-03--------\n");
        /*
         Write a program that asks user to enter a String
         Count how many special characters you have in the String

         "Hello World"      -> 0
         "Hello!"           -> 1
         "abc@gmail.com"    -> 2
         "!@#$%&"           -> 6
         */

        str = ScannerHelper.getString();
        int count = 0;

        for (int i = 0; i < str.length(); i++) {
            if (!Character.isLetterOrDigit(str.charAt(i)) && !Character.isWhitespace(str.charAt(i))) count++;
            }
        System.out.println(count);


        System.out.println("\n--------Task-04--------\n");
        /*
        Write a program that asks user to enter a String

        Count letters
        Count uppercase letters
        Count lowercase letters
        Count digits
        Count spaces
        Count specials

        Examples:
        The price of the iPhone is $1,000.

        Letters = 21
        Uppercase letters = 2
        Lowercase letters = 19
        Digits = 4
        Spaces = 6
        Specials = 3
         */

        int letters = 0, uppercase = 0, lowercase = 0, digits = 0, spaces = 0, specials = 0;

        str = ScannerHelper.getString();

        for (int i = 0; i < str.length(); i++) {
            char c = str.charAt(i);

            if (Character.isLetter(c)){
                letters++;
                if (Character.isUpperCase(c)){
                    uppercase++;
                } else {
                    lowercase++;
                }
            } else if(Character.isDigit(c)){
                digits++;
            } else if(Character.isWhitespace(c)) spaces++;
            else specials++;
        }
        System.out.println("Letters = " + letters);
        System.out.println("Uppercase = " + uppercase);
        System.out.println("Lowercase = " + lowercase);
        System.out.println("Digits = " + digits);
        System.out.println("Spaces = " + spaces);
        System.out.println("Specials = " + specials);

        System.out.println("\n--------Task-05--------\n");
        /*
        Write a program that asks user to enter a String
        Count how many words you have in the String

        "I like Java"

        words = spaces + 1

        3 words
         */

        int spaces1 = 0;

        for (int i = 0; i < str.length(); i++) {
            if (Character.isWhitespace(str.charAt(i)) && !Character.isWhitespace(str.charAt(i+1))) spaces1++;
        }

        System.out.println();

    }
}

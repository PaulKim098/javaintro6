package mathClass;

import java.util.Scanner;

public class FindMin {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Please enter 2 numbers:");
        int numb1 = scan.nextInt();
        int numb2 = scan.nextInt();

        System.out.println("The min of the given numbers is = " + Math.min(numb1, numb2));
        /*
        Program: Please enter 3 numbers
        User:
            5
            11
            15
        Program: The min of 5, 11, 15 is: 5
         */

        System.out.println("Please enter 3 numbers:");
        int num1 = scan.nextInt();
        int num2 = scan.nextInt();
        int num3 = scan.nextInt();

//        int min1 = Math.min(num1, num2);
//        int minFinal = Math.min(min1,num3);

        System.out.println("The min of " + num1 + ", " + num2 + ", " + num3 + " is: "
                + Math.min(Math.min(num1, num2), num3));

    }
}

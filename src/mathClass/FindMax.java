package mathClass;

public class FindMax {
    public static void main(String[] args) {
        System.out.println("Please enter two numbers");

        int numb1 = -10;
        int numb2 = -45;
        int max = Math.max(numb1, numb2);

        System.out.println(max);

        //Finding the max of 4 numbers

        int num1 = 2;
        int num2 = 8;
        int num3 = 5;
        int num4 = 35;

        // max between number1 and number2 = 8
        // max between number3 and number4 = 18

        int max1 = Math.max(num1, num2);
        int max2 = Math.max(num3, num4);
        int finalMax = Math.max(max1, max2);

        System.out.println(finalMax); // 18

        //Find the max of 3 numbers

        num1 = -30;
        num2 = -40;
        num3 = 0;

        max1 = Math.max(num1, num2);

        System.out.println(Math.max(max1, num3)); // 0


        // Finding the max of 5 numbers
        int a = 5;
        int b = 10;
        int c = 5;
        int d = 189;
        int e = 12;

        System.out.println(Math.max(Math.max(Math.max(a, b), Math.max(c,d)), e));
        max1 = Math.max(a, b); // (5, 10) -> 10
        max2 = Math.max(c, d); // (50, 189) -> 189
        int max3 = Math.max(max1, max2); // (10, 189) -> 189

        System.out.println(max3);

        System.out.println(Math.max(10 + 25, 45-10));

    }
}

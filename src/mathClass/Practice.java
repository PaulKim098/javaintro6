package mathClass;

import java.util.Scanner;

public class Practice {
    public static void main(String[] args) {

        System.out.println("\n---------Task-1--------\n");
        int randNum = (int) (Math.random() * 51);
        System.out.println(randNum);

        System.out.println("The random number * 5 = " + (randNum * 5));

        //System.out.println("The random number * 5 = " + ((int) (Math.random() * 51) * 5));

        System.out.println("\n---------Task-2--------\n");
        /*
        Requirement:
        Write a program that generates two random numbers
        between 1 and 10 (both 1 and 10 are included)

        Find the min number
        Find the max number
        Find the absolute difference of the numbers

        Expected result:
            Min number = {min}
            Max number = {max}
            Difference = {difference}

            int randNumber = (int) Math.round(Math.random() * 10);      X
            int randNumber = (int)(Math.random() * 10);                 X
            int randNumber = (int)(Math.random() * 9) + 1;              X
            int random = (int) (Math.random() * 10 + 1);                X
            int randNum1 = ((int)(Math.random() * 10) + 1);             Correct
            int randNum1 = ((int)(Math.round(Math.random() * 10)) + 1); Correct
         */

        int randNum1 = (int) (Math.random() * 10) + 1;
        int randNum2 = (int) (Math.random() * 10) + 1;

        System.out.println("The first random number = " + randNum1 + " and the second = " + randNum2);
        int min = Math.min(randNum1, randNum2);
        int max = Math.max(randNum1, randNum2);
        int absDiff = Math.abs(randNum1 - randNum2);

        System.out.println("Min number = " + min);
        System.out.println("Max number = " + max);
        System.out.println("Difference = " + absDiff);

        System.out.println("\n---------Task-3--------\n");

        int randomNum = (int) (Math.random()* 50) + 50;
        System.out.println(randomNum);
        int remainder = randomNum % 10;

        System.out.println("The random number % 10 = " + remainder);

        System.out.println("\n---------Task-4--------\n");
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter 5 numbers between 1 to 10:");

        int numb1 = scan.nextInt() * 5;
        int numb2 = scan.nextInt() * 4;
        int numb3 = scan.nextInt() * 3;
        int numb4 = scan.nextInt() * 2;
        int numb5 = scan.nextInt();
        int total = numb1 + numb2 + numb3 + numb4 + numb5;

        System.out.println(total);


//        System.out.println("\n---------Practice Task--------\n");
//        /*
//        Write a program that asks for
//        2 random numbers from 30 to 60.
//
//        Find the min number
//        Find the max number
//        Find the absolute difference of the numbers
//
//        Expected result:
//            Min number = {min}
//            Max number = {max}
//            Difference = {difference}
//         */

////        Scanner scan = new Scanner(System.in);
////        System.out.println("Please enter 2 numbers between 30 and 60:");
////        int randNum1 = scan.nextInt();
////        int randNum2 = scan.nextInt();
//
//        int randNum1 = (int) (Math.random() * 31) + 30;
//        int randNum2 = (int) (Math.random() * 31) + 30;
//
//        System.out.println(randNum1);
//        System.out.println(randNum2);
//
//        int min = Math.min(randNum1, randNum2);
//        int max = Math.max(randNum1, randNum2);
//        int diff = Math.abs(min - max);
//
//        System.out.println("Min number = " + min);
//        System.out.println("Max number = " + max);
//        System.out.println("Difference = " + diff);







    }
}

package variables;

public class UnderstandingVariables {
    public static void main(String[] args) {
        int age; // declaring a variable

        age = 50; // assigning or initializing a variable

        System.out.println(age);

        // capital letters and lowercase letter are seen differently with java
        double money = 5.5;
        double mOney = 4.5;
        double Money;

        double d1 = 10;
        System.out.println(d1);

        d1 = 15.5;

    }
}
